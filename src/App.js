import React from "react";
import "./App.css";
import Routes from "./routes";
import { ThemeProvider, CssBaseline } from "@material-ui/core";
import { ModalProvider } from "react-modal-hook";
import theme from "./theme";
import { hot } from "react-hot-loader/root";

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <ModalProvider>
        <CssBaseline />
        <Routes />
      </ModalProvider>
    </ThemeProvider>
  );
};

export default process.env.NODE_ENV === "development" ? hot(App) : App;
