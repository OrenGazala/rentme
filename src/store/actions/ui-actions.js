import { SAVE_TAB_INDEX } from "./types";

export const saveTabIndexAction = (tabIndex) => {
  return (dispatch, getState) => {
    dispatch({
      type: SAVE_TAB_INDEX,
      payload: {
        tabIndex,
      },
    });
  };
};
