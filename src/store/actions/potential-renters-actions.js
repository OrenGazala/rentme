import {
  GET_POTENTIAL_RENTERS,
  DELETE_RENT,
  ADD_RENT,
  DELETE_IMPRESSION,
  ON_FETCH_POTENTIAL_RENTERS,
} from "./types";

export const getPotentialRentersAction = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: GET_POTENTIAL_RENTERS,
      payload: {
        data,
      },
    });
  };
};

export const deleteRent = (id) => {
  return (dispatch, getState) => {
    dispatch({
      type: DELETE_RENT,
      payload: {
        id,
      },
    });
  };
};

export const deleteImpression = (id) => {
  return (dispatch, getState) => {
    dispatch({
      type: DELETE_IMPRESSION,
      payload: {
        id,
      },
    });
  };
};

export const addRent = (rentId, uid, enterUser) => {
  return (dispatch, getState) => {
    dispatch({
      type: ADD_RENT,
      payload: {
        rentId,
        uid,
        enterUser,
      },
    });
  };
};

export const onFetchPotentialRenters = () => {
  return (dispatch, getState) => {
    dispatch({
      type: ON_FETCH_POTENTIAL_RENTERS,
      payload: {},
    });
  };
};
