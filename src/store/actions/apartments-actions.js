import {
  GET_APARTMENTS,
  UPDATE_LIKE_APARTMENT,
  ON_FETCH_APARTMENTS,
  GET_LIKE_APARTMENT,
  GET_RENTS_APARTMENT,
} from "./types";

export const getApartmentsAction = (data, fromFilter = false, loading = false) => {
  return (dispatch, getState) => {
    dispatch({
      type: GET_APARTMENTS,
      payload: {
        data,
        fromFilter,
        loading,
      },
    });
  };
};

export const onFetchApartments = (loading) => {
  return (dispatch, getState) => {
    dispatch({
      type: ON_FETCH_APARTMENTS,
      payload: {
        loading,
      },
    });
  };
};

export const getLikeApartmentAction = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: GET_LIKE_APARTMENT,
      payload: {
        data,
      },
    });
  };
};

export const getRentsApartmentAction = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: GET_RENTS_APARTMENT,
      payload: {
        data,
      },
    });
  };
};

export const updateLikeApartmentAction = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: UPDATE_LIKE_APARTMENT,
      payload: {
        data,
      },
    });
  };
};
