import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux";
import { reduxFirestore, getFirestore } from "redux-firestore";
import { getFirebase } from "react-redux-firebase";
import fbConfig from "config/fb-conf";
import rootReducer from "./reducers/root-reducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const initialState = {};

const middleware = [
  thunk.withExtraArgument({
    getFirebase,
    getFirestore,
  }),
];

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["firestore", "firebase"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  initialState,
  composeEnhancers(applyMiddleware(...middleware), reduxFirestore(fbConfig))
);

export const persistor = persistStore(store);

export default { store, persistor };
