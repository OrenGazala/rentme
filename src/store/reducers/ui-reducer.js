import * as types from "../actions/types";

const initState = { tabIndex: 0 };

const uiReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SAVE_TAB_INDEX:
      return {
        ...state,
        tabIndex: action.payload.tabIndex,
      };
    default:
      return state;
  }
};

export default uiReducer;
