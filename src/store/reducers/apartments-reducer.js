import * as types from "../actions/types";

const initState = { data: {}, fromFilter: false, loading: true };

const apartmentsReducer = (state = initState, action) => {
  switch (action.type) {
    case types.GET_APARTMENTS:
      return { ...state, ...action.payload };

    case types.UPDATE_LIKE_APARTMENT:
      const likedApartments = { ...state.likedApartments };
      const { apartmentID, impression } = action.payload.data;

      if (likedApartments[apartmentID]) {
        likedApartments[apartmentID].impression = impression;
      } else {
        const apartment = state.data[apartmentID];
        likedApartments[apartmentID] = {
          ...apartment,
          impression,
        };
      }

      return {
        ...state,
        likedApartments,
      };

    case types.GET_LIKE_APARTMENT:
      return { ...state, likedApartments: action.payload.data };

    case types.GET_RENTS_APARTMENT:
      return { ...state, rentsApartments: action.payload.data };

    case types.ON_FETCH_APARTMENTS:
      return { ...state, loading: action.payload.loading };

    default:
      return state;
  }
};

export default apartmentsReducer;
