import { combineReducers } from "redux";
import { firebaseReducer } from "react-redux-firebase";
import { firestoreReducer } from "redux-firestore";
import { persistReducer } from "redux-persist";
import hardSet from "redux-persist/lib/stateReconciler/hardSet";
import storage from "redux-persist/lib/storage";
import uiReducer from "./ui-reducer";
import apartmentsReducer from "./apartments-reducer";
import potentialRentersReducer from "./potential-renters-reducer";

export default combineReducers({
  ui: uiReducer,
  apartments: apartmentsReducer,
  potentialRenters: potentialRentersReducer,
  firebase: persistReducer({ key: "firebaseState", storage, stateReconciler: hardSet }, firebaseReducer),
  firestore: persistReducer(
    { key: "firestoreState", storage, stateReconciler: hardSet, whitelist: ["data"] },
    firestoreReducer
  ),
});
