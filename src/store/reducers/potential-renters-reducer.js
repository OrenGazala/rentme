import * as types from '../actions/types';

const initState = { data: {}, loading: true };

const potentialRentersReducer = (state = initState, action) => {
  const { rents, impressions } = state.data;

  switch (action.type) {
    case types.GET_POTENTIAL_RENTERS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
      };

    case types.DELETE_RENT:
      delete rents[action.payload.id];
      return {
        ...state,
        data: {
          ...state.data,
          rents,
        },
      };

    case types.DELETE_IMPRESSION:
      delete impressions[action.payload.id];
      return {
        ...state,
        data: {
          ...state.data,
          impressions,
        },
      };

    case types.ADD_RENT:
      const uid = action.payload.uid;
      const user = action.payload.enterUser;
      const rentId = action.payload.rentId;
      return {
        ...state,
        data: {
          ...state.data,
          rents: { ...rents, [rentId]: { uid, user } },
        },
      };

    case types.ON_FETCH_POTENTIAL_RENTERS:
      return {
        ...state,
        loading: true,
      };

    default:
      return state;
  }
};

export default potentialRentersReducer;
