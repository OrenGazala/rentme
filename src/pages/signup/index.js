import React, { useEffect } from "react";
import { Avatar, CssBaseline, Typography, Container, Box } from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Copyright from "components/shared/copyright";
import useStyles from "./styles";
import SignUpForm from "components/layout/sign-forms/sign-up-form";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const SignUp = () => {
  const classes = useStyles();

  const { uid } = useSelector((state) => state.firebase.auth);
  const history = useHistory();

  useEffect(() => {
    if (uid) history.push("/");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
      </div>
      <SignUpForm />
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default SignUp;
