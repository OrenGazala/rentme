import React from "react";
import { useSelector } from "react-redux";
import { useFirestoreConnect, isLoaded, isEmpty } from "react-redux-firebase";
import CircularLoading from "components/shared/circular-loading";
import useStyles from "./styles";
import { Container, Typography, Button, Grid } from "@material-ui/core";
import { SentimentVeryDissatisfied } from "@material-ui/icons";
import { Link } from "react-router-dom";
import RentCard from "components/layout/rents/posts/card";
import { getApartmentsByUID } from "queries/apartments-queries";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import { APARTMENT_FROM } from "consts/route-consts";

const OwnApartments = () => {
  const { uid } = useSelector((state) => state.firebase.auth);
  useFirestoreConnect(getApartmentsByUID(uid));

  const classes = useStyles();

  const apartments = useSelector((state) => state.firestore.data.apartments);

  if (!isLoaded(apartments)) return <CircularLoading></CircularLoading>;

  if (isEmpty(apartments)) {
    return (
      <Container fixed maxWidth="md">
        <Typography component="h5" variant="h5" align="center" color="textPrimary" gutterBottom>
          Sorry, You don't have any apartments yet!
          <SentimentVeryDissatisfied fontSize="large" className={classes.sadEmoji} />
          <br />
          <Button color="secondary" variant="contained" component={Link} to="/add-apartment">
            Add your apartment right away!
          </Button>
        </Typography>
      </Container>
    );
  }

  return (
    <Container className={classes.cardGrid} maxWidth="lg">
      <Typography variant="h4" align="left" style={{ marginBottom: "24px" }}>
        <Button
          color="secondary"
          variant="contained"
          component={Link}
          to="/add-apartment"
          style={{ textTransform: "capitalize" }}
        >
          <AddCircleIcon style={{ marginRight: "4px" }} /> Add your apartment right away!
        </Button>
      </Typography>
      <Grid container spacing={4}>
        {Object.keys(apartments).map((id) => {
          return uid === apartments[id].uid ? (
            <RentCard key={id} id={id} from={APARTMENT_FROM.MY_APARTMENTS} />
          ) : (
            <div key={id}></div>
          );
        })}
      </Grid>
    </Container>
  );
};

export default OwnApartments;
