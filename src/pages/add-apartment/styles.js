import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    // top, left, bottom, right
    margin: theme.spacing(2, 0, 1, 9),
    backgroundColor: theme.palette.secondary.main,
  },
}));

export default useStyles;
