import React from "react";
import { Avatar, CssBaseline, Box, Typography, Container } from "@material-ui/core";
import { Hotel } from "@material-ui/icons";
import Copyright from "components/shared/copyright";
import useStyles from "./styles";
import AddApartmentForm from "components/layout/apartments/add";

const AddApartment = () => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          <Avatar className={classes.avatar}>
            <Hotel />
          </Avatar>
          Add Apartments
        </Typography>
      </div>
      <AddApartmentForm />
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default AddApartment;
