import React, { useEffect } from "react";
import Footer from "components/shared/footer";

import SearchFilters from "components/layout/rents/search-filters";
import RentPosts from "components/layout/rents/posts";
import { useSelector, useDispatch } from "react-redux";
import useFunctions from "hooks/firebase/useFunctions";
import {
  GET_APARTMENTS_BY_RENTER_PROPS,
  GET_APARTMENTS_BY_RANDOM,
  GET_LIKED_APARTMENTS,
} from "consts/firebase-functions";
import {
  getApartmentsAction,
  onFetchApartments,
  getLikeApartmentAction,
} from "store/actions/apartments-actions";

const RentsFeed = () => {
  const functions = useFunctions();
  const dispatch = useDispatch();

  const { uid } = useSelector((state) => state.firebase.auth);
  const { profile } = useSelector((state) => state.firebase);
  const apartments = useSelector((state) => state.apartments.data);
  const fromFilter = useSelector((state) => state.apartments.fromFilter);

  const hasRenterProps = () =>
    uid &&
    profile.renterProps.city.id &&
    profile.renterProps.propertyType &&
    profile.renterProps.bedrooms;

  const isRefresh = () => !Object.keys(apartments).length || fromFilter;

  useEffect(() => {
    if (isRefresh()) {
      dispatch(onFetchApartments(true));
      if (hasRenterProps()) {
        functions.call(GET_APARTMENTS_BY_RENTER_PROPS).then((res) => dispatch(getApartmentsAction(res)));
      } else {
        functions.call(GET_APARTMENTS_BY_RANDOM).then((res) => dispatch(getApartmentsAction(res)));
      }
      functions.call(GET_LIKED_APARTMENTS).then((res) => dispatch(getLikeApartmentAction(res)));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [uid]);

  return (
    <React.Fragment>
      {uid ? <SearchFilters /> : <></>}
      <RentPosts />
      <Footer />
    </React.Fragment>
  );
};

export default RentsFeed;
