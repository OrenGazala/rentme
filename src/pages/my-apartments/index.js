import React from "react";
import { useSelector } from "react-redux";
import { useFirestoreConnect } from "react-redux-firebase";
import { Container, Tabs, Tab } from "@material-ui/core";
import { HomeWork, House } from "@material-ui/icons";
import { getApartmentsByUID } from "queries/apartments-queries";
import OwnApartments from "pages/own-apartments";
import MyRents from "pages/my-rents";

const MyApartments = () => {
  const { uid } = useSelector((state) => state.firebase.auth);
  useFirestoreConnect(getApartmentsByUID(uid));

  const [tab, setTab] = React.useState(0);
  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  return (
    <React.Fragment>
      <Container fixed maxWidth="md">
        <Tabs
          value={tab}
          onChange={handleChange}
          variant="fullWidth"
          indicatorColor="secondary"
          textColor="secondary"
          aria-label="icon label tabs example"
        >
          <Tab icon={<HomeWork />} label="Your Apartments" />
          <Tab icon={<House />} label="Your Rents" />
        </Tabs>
      </Container>
      {tab === 0 ? <OwnApartments /> : <MyRents />}
    </React.Fragment>
  );
};

export default MyApartments;
