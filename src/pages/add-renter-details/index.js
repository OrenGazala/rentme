import React from "react";
import { CssBaseline, Container, Box, Typography, Avatar } from "@material-ui/core";
import Copyright from "components/shared/copyright";
import AddRenterDetailForm from "components/layout/sign-forms/add-renter-details-form";
import useStyles from "./styles";
import { Person } from "@material-ui/icons";

const AddRenterDetails = () => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          <Avatar className={classes.avatar}>
            <Person />
          </Avatar>
          Tell us About you
        </Typography>
      </div>
      <AddRenterDetailForm />
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default AddRenterDetails;
