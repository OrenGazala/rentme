import React from "react";
import { Container, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import useStyles from "./styles";

const NotFound = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="xl" className={classes.root}>
      <div className={classes.notfound}>
        <div className={classes.oops}>
          <h1 className={classes.magic}>Oops!</h1>
        </div>
        <h2 className={classes.title}>404 - Page not found</h2>
        <p className={classes.help}>
          The page you are looking for might have been removed had its name changed or is temporarily
          unavailable.
        </p>
        <Button className={classes.button} component={Link} to="/">
          Go To Homepage
        </Button>
      </div>
    </Container>
  );
};

export default NotFound;
