import { makeStyles } from "@material-ui/core/styles";
import Notfound from "assets/images/notfound.jpg";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    height: "90vh",
    backgroundColor: "white",
    zIndex: 5400000,
  },
  notfound: {
    maxWidth: "390px",
    width: "100%",
    textAlign: "center",
    position: "absolute",
    left: "50%",
    top: "50%",
    WebkitTransform: "translate(-50%, -50%)",
    MsTransform: "translate(-50%, -50%)",
    transform: "translate(-50%, -50%)",
  },
  oops: {
    height: window.innerWidth < 767 ? "120px" : "280px",
    position: "relative",
    zIndex: -1,
  },
  magic: {
    fontFamily: "Montserrat , sans-serif",
    fontSize: window.innerWidth < 767 ? "80px" : "200px",
    margin: "0px",
    fontWeight: 900,
    position: "absolute",
    left: "50%",
    WebkitTransform: "translateX(-50%)",
    MsTransform: "translateX(-50%)",
    transform: "translateX(-50%)",
    background: `url(${Notfound}) no-repeat`,
    WebkitBackgroundClip: "text",
    WebkitTextFillColor: "transparent",
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  title: {
    fontFamily: "Montserrat , sans-serif",
    color: "#000",
    fontSize: "24px",
    fontWeight: 700,
    textTransform: "uppercase",
    marginTop: theme.spacing(2),
  },
  help: {
    fontFamily: "Montserrat , sans-serif",
    color: "#000",
    fontSize: "14px",
    fontWeight: 400,
    marginBottom: "20px",
    marginTop: "0px",
  },
  button: {
    textDecoration: "none",
    textTransform: "uppercase",
    background: "#673AB7",
    display: "inline-block",
    padding: "15px 30px",
    borderRadius: "40px",
    color: "black",
    fontWeight: 700,
    boxShadow: "0px 4px 15px -5px #0046d5 !important",
  },
}));

export default useStyles;
