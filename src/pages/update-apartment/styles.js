import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    // top, left, bottom, right
    // margin: theme.spacing(2, 0, 1, 9),
    width: "60px",
    height: "60px",
    backgroundColor: theme.palette.secondary.main,
  },
}));

export default useStyles;
