import React from "react";
import { Avatar, CssBaseline, Typography, Container } from "@material-ui/core";

import EditIcon from "@material-ui/icons/Edit";
import useStyles from "./styles";
import AddApartmentForm from "components/layout/apartments/add";
import { useParams } from "react-router-dom";

const UpdateApartment = () => {
  const classes = useStyles();
  const { id } = useParams();

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <EditIcon fontSize="large" />
        </Avatar>
        <Typography component="h5" variant="h5" align="center">
          Edit Your Apartment Here
        </Typography>
      </div>
      <AddApartmentForm apartmentId={id} />
    </Container>
  );
};

export default UpdateApartment;
