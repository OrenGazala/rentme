import React from "react";
import { useSelector } from "react-redux";
import { useFirestoreConnect, isLoaded, isEmpty } from "react-redux-firebase";
import CircularLoading from "components/shared/circular-loading";
import useStyles from "./styles";
import { Container, Typography, Button, Grid } from "@material-ui/core";
import { SentimentVeryDissatisfied } from "@material-ui/icons";
import { Link } from "react-router-dom";
import RentCard from "components/layout/rents/posts/card";
import { getImpressionByUID } from "queries/apartment-impressions-queries";
import { APARTMENT_FROM } from "consts/route-consts";

const FavoriteApartments = () => {
  const { uid } = useSelector(({ firebase: { auth } }) => auth);
  useFirestoreConnect(getImpressionByUID(uid));

  const classes = useStyles();
  const impressions = useSelector((state) => state.firestore.data.apartment_impressions);

  if (!isLoaded(impressions)) return <CircularLoading />;

  if (isEmpty(impressions)) {
    return (
      <Container fixed maxWidth="md">
        <Typography component="h5" variant="h5" align="center" color="textPrimary" gutterBottom>
          Sorry, You don't liked any apartments yet!
          <SentimentVeryDissatisfied fontSize="large" className={classes.sadEmoji} />
          <br />
          <Button color="primary" variant="contained" component={Link} to="/">
            Search for apartment right away!
          </Button>
        </Typography>
      </Container>
    );
  }

  return (
    <Container className={classes.cardGrid} maxWidth="lg">
      <Grid container spacing={4}>
        {Object.values(impressions)
          .filter((impression) => (impression ? impression : false))
          .map((impression) => (
            <RentCard
              key={impression.apartmentID}
              id={impression.apartmentID}
              from={APARTMENT_FROM.LIKED_APARTMENTS}
            />
          ))}
      </Grid>
    </Container>
  );
};

export default FavoriteApartments;
