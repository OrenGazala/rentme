import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  heroContent: {
    padding: theme.spacing(3, 0, 1),
  },
  title: {
    width: "fit-content",
    margin: "auto",
    padding: "5px 30px",
  },
  money: {
    padding: theme.spacing(0, 0),
    display: "inline-flex",
    fontWeight: 700,
  },
  rating: {
    padding: "3px 0px 3px 30px",
    alignSelf: "center",
  },
  chip: {
    margin: theme.spacing(1),
  },
  userContact: {
    backgroundColor: "#90caf9ed",
    borderRadius: "50px",
    padding: "2px !important",
    height: "fit-content",
    marginTop: theme.spacing(6),
  },
  icon: {
    marginBottom: "-2px",
  },
  details: {
    padding: theme.spacing(1, 0),
  },
  picture: {
    width: "100%",
    height: "400px",
    padding: theme.spacing(1, 0),
  },
  margin: {
    marginTop: theme.spacing(5),
  },
  description: {},
}));

export default useStyles;
