import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { APARTMENTS_COLLECTION } from "consts/collections";
import { useSelector } from "react-redux";

import Carousel from "react-material-ui-carousel";

import { Container, Typography, Grid, Chip, Divider, Box, Hidden } from "@material-ui/core";

import {
  School,
  LocalParking,
  DirectionsBus,
  Height,
  Deck,
  AcUnit,
  Weekend,
  ChildCare,
  MenuBook,
  LocalBar,
  SmokingRooms,
  Pets,
  LocationCity,
} from "@material-ui/icons";

import { BathtubRounded, LocalHotelRounded } from "@material-ui/icons";
import { Home, Room } from "@material-ui/icons";

import Rating from "@material-ui/lab/Rating";
import ShowFeedbacks from "components/layout/feedbacks/show";
import useStyles from "./styles";
import ApartmentOwnerDetails from "components/layout/apartments/owner-details";
import "./styles.css";
import Like from "components/layout/apartments/like";

import defaultImage from "assets/images/noImage.jpg";
import AddFeedback from "components/layout/feedbacks/add";
import { APARTMENT_FROM } from "consts/route-consts";

const apartmentDetailsChips = {
  hasParking: { header: "Parking", icon: <LocalParking /> },
  hasPublicTransport: { header: "Public Transport", icon: <DirectionsBus /> },
  hasElevator: { header: "Elevator", icon: <Height /> },
  hasBalcony: { header: "Balcony", icon: <Deck /> },
  hasAirConditioning: { header: "Air Conditioning", icon: <AcUnit /> },
  hasFurnitures: { header: "Furnitures", icon: <Weekend /> },
  hasSchool: { header: "School", icon: <School /> },
  hasKinderGardens: { header: "KinderGardens", icon: <ChildCare /> },
  hasSynagogue: { header: "Synagogue", icon: <MenuBook /> },
  hasBars: { header: "Bars", icon: <LocalBar /> },
  allowSmoke: { header: "Allow Smoke", icon: <SmokingRooms /> },
  allowPet: { header: "Allow Pet", icon: <Pets /> },
};

const ApartmentDetails = () => {
  const classes = useStyles();
  const { id, from } = useParams();
  const { uid } = useSelector((state) => state.firebase.auth);

  const apartment = useSelector((state) => {
    switch (+from) {
      case APARTMENT_FROM.MY_APARTMENTS:
        return state.firestore.data.apartments && state.firestore.data.apartments[id];
      case APARTMENT_FROM.LIKED_APARTMENTS:
        return state.apartments.likedApartments && state.apartments.likedApartments[id];
      case APARTMENT_FROM.HOME:
        return state.apartments.data && state.apartments.data[id];
      case APARTMENT_FROM.RENTS_APARTMENTS:
        return state.apartments.rentsApartments && state.apartments.rentsApartments[id];
      default:
        break;
    }
  });

  const [color, setColor] = useState("none");
  const [activated, setActivted] = useState([]);
  const [pictures, setPictures] = useState([]);

  useEffect(() => {
    let active, tempPictures;

    if (from === APARTMENT_FROM.MY_APARTMENTS) {
      active = Object.keys(apartment).filter(
        (key) => apartment[key] === true && key !== "active" && key !== "like"
      );
      if (active) setActivted(active);

      tempPictures = apartment.images.length ? apartment.images : [{ url: defaultImage }];

      if (tempPictures) setPictures(tempPictures);
    } else {
      if (apartment.matchLevel >= 8) setColor("#21ff21");
      else if (apartment.matchLevel >= 5) setColor("#ffd600");
      else setColor("#fd2500");

      active = Object.keys(apartment).filter(
        (key) => apartment[key] === true && key !== "active" && key !== "like"
      );
      if (active) setActivted(active);

      tempPictures = apartment.images.length ? apartment.images : [{ url: defaultImage }];
      if (tempPictures) setPictures(tempPictures);
    }
  }, [apartment, from]);

  return apartment ? (
    <React.Fragment>
      <Container fixed maxWidth="lg">
        <div className={classes.heroContent}>
          {uid !== id && uid !== apartment?.uid && +from !== APARTMENT_FROM.RENTS_APARTMENTS ? (
            <Typography component="div" style={{ display: "inline-flex" }}>
              <Like uid={uid} apartmentID={id} positionStyle={"relative"} />
              <Box style={{ alignSelf: "center", paddingLeft: "10px" }}>
                Do you like this apartment? Let Us Know!
              </Box>
            </Typography>
          ) : (
            <></>
          )}
        </div>
        <Grid container spacing={3}>
          <Grid item xs={12} md={7} className={classes.picture}>
            <Carousel autoPlay interval={4000} indicators animation="fade">
              {pictures.map((pic) => (
                <img key={pic} className={classes.picture} src={pic.url} alt="wisherlogo" />
              ))}
            </Carousel>
          </Grid>
          <Grid item xs={12} md={5} className={classes.details}>
            <Typography variant="h6" color="textPrimary" gutterBottom>
              <Room className={classes.icon} color="primary" /> {apartment.city.region}
            </Typography>
            <Typography variant="h6" color="textPrimary" gutterBottom>
              <LocationCity className={classes.icon} color="primary" /> {apartment.city.name} ,{" "}
              {apartment.address}
            </Typography>
            <Typography variant="h6" align="left" color="textPrimary" gutterBottom>
              <Home className={classes.icon} color="primary" /> {apartment.propertyType}
            </Typography>
            <Typography variant="h6" color="textPrimary" gutterBottom className={classes.details}>
              <Grid container spacing={2}>
                <Grid item>
                  <LocalHotelRounded className={classes.icon} color="primary" /> {apartment.bedrooms}{" "}
                  Bedrooms
                </Grid>
                <Grid item>
                  <BathtubRounded className={classes.icon} color="primary" /> {apartment.bathrooms}{" "}
                  Bathrooms
                </Grid>
              </Grid>
            </Typography>
            <Typography variant="h4" component="h4" gutterBottom className={classes.money}>
              {/* <AttachMoney
                style={{ marginTop: "4px", marginLeft: "-2px", fontSize: "smaller" }}
                color="primary"
              /> */}
              {"$"}
              {apartment.price}
              <Rating
                className={classes.rating}
                name="hover-feedback"
                value={Math.round(apartment.feedbackDetails.avg * 2) / 2}
                precision={0.5}
                size="large"
                readOnly
              />

              {apartment.matchLevel && color !== "none" ? (
                <Hidden smDown>
                  <Box
                    style={{
                      color: color,
                      fontSize: "1.0rem",
                      paddingLeft: "1rem",
                    }}
                  >
                    <span style={{ paddingLeft: "34px" }}>{apartment.matchLevel}</span>
                    <Box>Level Match</Box>
                  </Box>
                </Hidden>
              ) : (
                <></>
              )}
            </Typography>
            <ApartmentOwnerDetails uid={apartment.uid} />

            <Divider />
            <Typography variant="body1" align="left" color="textPrimary" gutterBottom component="div">
              {activated.map((label, index) => (
                <Chip
                  key={index}
                  clickable
                  className={classes.chip}
                  icon={apartmentDetailsChips[label].icon}
                  label={
                    apartmentDetailsChips[label].header
                      ? apartmentDetailsChips[label].header
                      : " הנתון לא נכון. תשנה בהתאם את הדיבי "
                  }
                  variant="outlined"
                  color="primary"
                />
              ))}
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Typography variant="h4" align="left" gutterBottom className={classes.margin}>
              Description
            </Typography>
            <Divider />
            <Typography variant="body1" align="left" className={classes.description} gutterBottom>
              {apartment.description}
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.margin}>
            <Typography variant="h4" align="left" color="textPrimary" gutterBottom>
              Feedbacks
            </Typography>
            <Divider />
            <AddFeedback collection={APARTMENTS_COLLECTION} uid={uid} apartmentID={id} />
            <ShowFeedbacks collection={APARTMENTS_COLLECTION} doc={id} />
          </Grid>
        </Grid>
      </Container>
    </React.Fragment>
  ) : (
    <></>
  );
};

export default ApartmentDetails;
