import React, { useEffect } from "react";
import { CssBaseline, Container } from "@material-ui/core";
import PotentialRenters from "components/layout/apartments/potential-renters";
import { useParams } from "react-router-dom";
import { GET_ALL_POTENTIAL_RENTERS } from "consts/firebase-functions";
import {
  getPotentialRentersAction,
  onFetchPotentialRenters,
} from "store/actions/potential-renters-actions";
import useFunctions from "hooks/firebase/useFunctions";
import { useDispatch, useSelector } from "react-redux";
import CircularLoading from "components/shared/circular-loading";

const PotentialRentersPage = () => {
  const { apartmentID } = useParams();
  const functions = useFunctions();
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.potentialRenters.loading);

  useEffect(() => {
    dispatch(onFetchPotentialRenters());
    functions.call(GET_ALL_POTENTIAL_RENTERS, { apartmentID }).then((res) => {
      dispatch(getPotentialRentersAction(res));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) return <CircularLoading />;

  return (
    <Container component="main">
      <CssBaseline />
      <PotentialRenters />
    </Container>
  );
};

export default PotentialRentersPage;
