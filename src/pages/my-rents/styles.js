import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  sadEmoji: {
    position: "relative",
    top: "7px",
    left: "7px",
  },
}));

export default useStyles;
