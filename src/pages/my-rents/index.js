import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import useStyles from "./styles";
import { Container, Grid } from "@material-ui/core";

import RentCard from "components/layout/rents/posts/card";
import useFunctions from "hooks/firebase/useFunctions";
import { GET_RENTS_APARTMENTS } from "consts/firebase-functions";
import { getRentsApartmentAction } from "store/actions/apartments-actions";
import { APARTMENT_FROM } from "consts/route-consts";

const MyRents = () => {
  const functions = useFunctions();
  const dispatch = useDispatch();

  useEffect(() => {
    functions.call(GET_RENTS_APARTMENTS).then((res) => dispatch(getRentsApartmentAction(res)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const rentsApartments = useSelector((state) => state.apartments.rentsApartments);
  const classes = useStyles();
  if (!rentsApartments) return <></>;

  return (
    <Container className={classes.cardGrid} maxWidth="lg">
      <Grid container spacing={4}>
        {Object.keys(rentsApartments).map((id) => (
          <RentCard key={id} id={id} from={APARTMENT_FROM.RENTS_APARTMENTS} />
        ))}
      </Grid>
    </Container>
  );
};

export default MyRents;
