import React from "react";
import { useSelector } from "react-redux";
import { isLoaded } from "react-redux-firebase";
import RentsFeed from "pages/rents-feed";

const Home = () => {
  const profile = useSelector((state) => state.firebase.profile);
  if (!isLoaded(profile)) return <></>;
  return <RentsFeed></RentsFeed>;
};

export default Home;
