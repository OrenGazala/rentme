import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: "10px",
    height: "100%",
  },

  personalDetails: {
    display: "grid",
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
  },
  review: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  typography: {
    textAlign: "center",
  },
}));

export default useStyles;
