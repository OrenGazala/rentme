import React from "react";
import useStyles from "./styles";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { USERS_COLLECTION } from "consts/collections";
import ProfileDetails from "components/layout/profile/profile-details";
import RenterDetails from "components/layout/profile/renter-details";
import { SentimentVeryDissatisfied } from "@material-ui/icons";
import { Grid, Typography, Container } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import ShowFeedbacks from "components/layout/feedbacks/show";
import { useFirestoreConnect, isLoaded, isEmpty } from "react-redux-firebase";
import CircularLoading from "components/shared/circular-loading";
import { getUserById } from "queries/user-queries";

const UserProfile = () => {
  const { id } = useParams();
  const classes = useStyles();

  useFirestoreConnect(getUserById(id));

  const profile = useSelector((state) => state.firestore.data.users && state.firestore.data.users[id]);
  if (!isLoaded(profile)) return <CircularLoading />;

  if (isEmpty(profile)) {
    return (
      <Container fixed maxWidth="md">
        <Typography component="h5" variant="h5" align="center" color="textPrimary" gutterBottom>
          Sorry, Didn't find this user!
          <SentimentVeryDissatisfied fontSize="large" className={classes.sadEmoji} />
          <br />
        </Typography>
      </Container>
    );
  }

  return (
    <div className={classes.root}>
      <br></br>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <Paper elevation={3} className={classes.paper}>
              <ProfileDetails id={id} />
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper elevation={3} className={classes.paper}>
              <Typography variant="h5" className={classes.typography}>
                Renter Details
              </Typography>
              <br></br>
              <RenterDetails id={id} />
            </Paper>
          </Grid>
          <Grid item xs={12} md={12}>
            <Paper elevation={3} className={classes.paper}>
              <Typography color="textSecondary" variant="h6" className={classes.typography}>
                Reviews
              </Typography>
              <ShowFeedbacks collection={USERS_COLLECTION} doc={id} />
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default UserProfile;
