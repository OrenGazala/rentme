import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navbar from "components/shared/navbar";

import Bottombar from "components/shared/bottombar";
import * as pages from "../pages";
import ProtectedRoute from "./protected-route";
import ScrollToTop from "components/shared/scroll-to-top";

const Routes = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <ScrollToTop>
        <Switch>
          {/* public routes */}
          <Route exact path="/" component={pages.Home} />
          <Route path="/signup" component={pages.SignUp} />
          <Route path="/signin" component={pages.SignIn} />

          {/* private routes */}
          <ProtectedRoute exact path="/add-apartment" component={pages.AddApartment} />
          <ProtectedRoute exact path="/add-apartment/:id" component={pages.UpdateApartment} />
          <ProtectedRoute path="/user-profile/:id" component={pages.UserProfile} />
          <ProtectedRoute path="/my-apartments" component={pages.MyApartments} />
          <ProtectedRoute path="/favorite-apartments" component={pages.FavoriteApartments} />
          <ProtectedRoute path="/apartment/:id/:from" component={pages.ApartmentDetails} />
          <ProtectedRoute path="/add-renter-details" component={pages.AddRenterDetails} />
          <ProtectedRoute path="/potential-renters/:apartmentID" component={pages.PotentialRenters} />

          <Route path="/*" component={pages.NotFound} />
        </Switch>
      </ScrollToTop>
      <div style={{ marginTop: window.innerWidth > 600 ? "0px" : "48px" }}>
        <Bottombar />
      </div>
    </BrowserRouter>
  );
};

export default Routes;
