import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ component, publicRoute, ...rest }) => {
  const { uid } = useSelector((state) => state.firebase.auth);

  return (
    <Route
      {...rest}
      render={(props) => (uid ? React.createElement(component, props) : <Redirect to="/" />)}
    />
  );
};

export default ProtectedRoute;
