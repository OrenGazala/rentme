export const propertyTypes = {
  APARTMENT: "Apartment",
  GARDEN_APARTMENT: "Garden Apartment",
  VILA: "Vila",
  PENTHOUSE: "Penthouse",
  DUPLEX: "Duplex",
};

export const partnersMarks = [
  { value: 1, label: "1" },
  { value: 4, label: "4" },
  { value: 7, label: "7" },
  { value: 10, label: "10" },
];

export const bathroomsMarks = [
  { value: 0, label: "0" },
  { value: 2, label: "2" },
  { value: 4, label: "4" },
  { value: 6, label: "6" },
  { value: 8, label: "8" },
  { value: 10, label: "10" },
];

export const bedroomsMarks = [
  { value: 0.5, label: "0.5" },
  { value: 2, label: "2" },
  { value: 4, label: "4" },
  { value: 6, label: "6" },
  { value: 8, label: "8" },
  { value: 10, label: "10" },
];

export const priceMarks = [
  { value: 0, label: "$0" },
  { value: 2500, label: "$2500" },
  { value: 5000, label: "$5000" },
  { value: 7500, label: "$7500" },
  { value: 10000, label: "$10000" },
];
