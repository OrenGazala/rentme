export const USERS_COLLECTION = "users";
export const APARTMENTS_COLLECTION = "apartments";
export const APARTMENT_IMPRESSIONS_COLLECTION = "apartment_impressions";
export const FEEDBACKS_COLLECTION = "feedbacks";
export const RENTS_COLLECTION = "rents";
