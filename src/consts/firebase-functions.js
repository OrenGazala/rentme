export const GET_APARTMENTS_BY_RENTER_PROPS = "functions-apartments-getByRenterProps";
export const GET_APARTMENTS_BY_RANDOM = "functions-apartments-getByRandom";
export const GET_APARTMENTS_BY_FILTER = "functions-apartments-getByFilter";
export const GET_ALL_POTENTIAL_RENTERS = "functions-potentialRenters-getAll";
export const GET_LIKED_APARTMENTS = "functions-apartments-getLikedApartments";
export const GET_RENTS_APARTMENTS = "functions-apartments-getMyRents";
