export const SALARIES = {
  VERY_LOW: "0 ~ 4K",
  LOW: "4K ~ 8K",
  MEDIUM: "8K ~ 12K",
  HIGH: "12K ~ 15K",
  VERY_HIGH: "15K+",
};

export const EDUCATIONS = {
  ELEMENTRY_SCHOOL: "Elementry school",
  HIGH_SCHOOL: "High School",
  COLLEAGE: "College",
  BACHELOR_DEGREE: "Bachelor degree",
  ASSOCIATE_DEGREE: "Associate degree",
  DOCTORATE_DEGREE: "Doctorate degree",
};

export const FAMILY_STATUSES = {
  SINGLE: "Single",
  RELATIONSHIP: "Relationship",
  MARRIED: "Married",
  DIVORCED: "Divorced",
  WIDOW: "Widow",
};
