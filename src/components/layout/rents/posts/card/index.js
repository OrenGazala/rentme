import React from "react";
import useStyles from "./styles";
import { Link as RouterLink } from "react-router-dom";
import {
  Card,
  CardContent,
  CardMedia,
  Box,
  Typography,
  CardActionArea,
  Grid,
  Divider,
  Link,
} from "@material-ui/core";
import { BathtubRounded, LocalHotelRounded } from "@material-ui/icons";
import Rating from "@material-ui/lab/Rating";
import { useSelector } from "react-redux";
import defaultImage from "assets/images/noImage.jpg";
import LandlordCardActions from "./actions/landlord";
import Like from "components/layout/apartments/like";
import { LocationCity } from "@material-ui/icons";
import { Home, Room } from "@material-ui/icons";
import { APARTMENT_FROM } from "consts/route-consts";

const RentCard = ({ id, from }) => {
  const classes = useStyles();
  const { uid } = useSelector((state) => state.firebase.auth);

  const apartment = useSelector((state) => {
    switch (+from) {
      case APARTMENT_FROM.MY_APARTMENTS:
        return state.firestore.data.apartments && state.firestore.data.apartments[id];
      case APARTMENT_FROM.LIKED_APARTMENTS:
        return state.apartments.likedApartments && state.apartments.likedApartments[id];
      case APARTMENT_FROM.RENTS_APARTMENTS:
        return state.apartments.rentsApartments && state.apartments.rentsApartments[id];
      case APARTMENT_FROM.HOME:
        return state.apartments.data && state.apartments.data[id];
      default:
        break;
    }
  });

  const getColor = () => {
    if (apartment.matchLevel >= 8) return "#21ff21";
    else if (apartment.matchLevel >= 5) return "#ffd600";
    else return "#fd2500";
  };

  if (!apartment) return <div></div>;

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card raised={true} className={classes.card}>
        <CardActionArea>
          <Link
            component={RouterLink}
            to={uid ? `/apartment/${id}/${from}` : "/signin"}
            underline="none"
            color="inherit"
          >
            <CardMedia
              className={classes.cardMedia}
              component="div"
              image={apartment.images.length ? apartment.images[0].url : defaultImage}
              title="Image title"
            />
            <CardContent className={classes.cardContent}>
              <Typography variant="h5" gutterBottom>
                {/* // style={{ textAlign: "center" }}> */}
                <Room className={classes.icon} color="primary" />
                {apartment.city.region}
              </Typography>
              <Typography
                className={classes.cardTitle}
                gutterBottom
                variant="h5"
                component="div"
                style={{ position: "relative" }}
              >
                <LocationCity className={classes.icon} color="primary" />
                <span style={{ marginLeft: "-2px" }}> {apartment.city.name}</span>
                {uid && uid !== apartment.uid && apartment.matchLevel ? (
                  <Box
                    style={{
                      color: getColor(),
                      position: "absolute",
                      top: "-8px",
                      fontSize: "1.0rem",
                      right: "0px",
                    }}
                  >
                    <span style={{ paddingLeft: "34px" }}>{apartment.matchLevel}</span>
                    <Box>Level Match</Box>
                  </Box>
                ) : (
                  <></>
                )}
              </Typography>
              <Typography variant="h6" gutterBottom style={{ height: "64px" }}>
                <Home className={classes.icon} color="primary" />
                {apartment.address} - {apartment.propertyType}
              </Typography>
              <Typography variant="h4" component="div" gutterBottom>
                {`$ ${apartment.price}`}
                <Rating
                  name="hover-feedback"
                  value={Math.round(apartment.feedbackDetails.avg * 2) / 2}
                  precision={0.5}
                  readOnly
                  style={{ padding: "0px 8px" }}
                />
              </Typography>

              <Divider light />
              <br />
              <Typography
                className={classes.details}
                color="textSecondary"
                variant={window.innerWidth > 576 ? "body1" : "body2"}
                component="div"
                align="center"
              >
                <Grid item xs={6}>
                  <LocalHotelRounded className={classes.iconDetails} />
                  {apartment.bedrooms} Bedrooms
                </Grid>
                <Grid item xs={6}>
                  <BathtubRounded className={classes.iconDetails} />
                  {apartment.bathrooms} Bathrooms
                </Grid>
              </Typography>
            </CardContent>
          </Link>
        </CardActionArea>
        {/* <Divider light /> */}
        {uid && +from !== APARTMENT_FROM.RENTS_APARTMENTS ? (
          uid !== apartment.uid ? (
            <Like uid={uid} apartmentID={id} positionStyle={"absolute"} />
          ) : (
            <LandlordCardActions id={id} />
          )
        ) : (
          <></>
        )}
      </Card>
    </Grid>
  );
};

export default RentCard;
