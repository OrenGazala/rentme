import React, { useState, useEffect } from "react";
import { Grid, IconButton, Tooltip, Zoom, Switch, CardActions, Badge } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import useStyles from "components/layout/rents/posts/card/styles";
import { DeleteRounded } from "@material-ui/icons";
import EditIcon from "@material-ui/icons/Edit";
import { useSelector } from "react-redux";
import useCollection from "hooks/firebase/useCollection";
import { APARTMENTS_COLLECTION } from "consts/collections";
import RecentActorsIcon from "@material-ui/icons/RecentActors";
import { useFirestoreConnect } from "react-redux-firebase";
import { getImpressionByApartment } from "queries/apartment-impressions-queries";

const LandlordCardActions = ({ id }) => {
  const classes = useStyles();
  const history = useHistory();
  const db = useCollection();
  useFirestoreConnect(getImpressionByApartment(id));

  const { active } = useSelector((state) => state.firestore.data.apartments[id]);

  const storeAs = `apartment_impressions_${id}`;
  const apartmentImpressions = useSelector((state) => state.firestore.data[storeAs]);
  const [potentialRenters, setPotentialRenters] = useState(0);

  useEffect(() => {
    if (apartmentImpressions) setPotentialRenters(Object.keys(apartmentImpressions).length);
  }, [apartmentImpressions]);

  const handleActiveChange = async () => {
    await db.update({
      collection: APARTMENTS_COLLECTION,
      doc: id,
      data: { active: !active },
    });
  };

  const deleteApartment = async () => {
    await db.remove({
      collection: APARTMENTS_COLLECTION,
      doc: id,
    });
  };

  const editApartment = () => {
    history.push(`/add-apartment/${id}`);
  };

  const showPotentialRenters = () => {
    history.push(`/potential-renters/${id}`);
  };

  return (
    <CardActions>
      {/* className={classes.cardAction} */}
      <Grid item xs={8}>
        <Switch checked={active} onChange={handleActiveChange} color="secondary" />
        <Tooltip title="Potential Renters" placement="bottom" TransitionComponent={Zoom} arrow>
          <IconButton color="inherit" onClick={showPotentialRenters}>
            <Badge badgeContent={potentialRenters} color="secondary">
              <RecentActorsIcon fontSize="large" />
            </Badge>
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs={2} className={classes.marginAction}>
        <Tooltip title="Edit Your Post" placement="bottom" TransitionComponent={Zoom} arrow>
          <IconButton aria-label="edit" onClick={editApartment}>
            <EditIcon />
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs={2} className={classes.marginAction}>
        <Tooltip title="Delete Post" placement="bottom" TransitionComponent={Zoom} arrow>
          <IconButton
            aria-label="delete post"
            className={classes.marginAction}
            onClick={deleteApartment}
          >
            <DeleteRounded />
          </IconButton>
        </Tooltip>
      </Grid>
    </CardActions>
  );
};

export default LandlordCardActions;
