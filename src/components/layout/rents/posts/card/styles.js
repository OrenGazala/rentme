import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    position: "relative",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  cardTitle: {
    // position: "absolute",
    // width: "70%",
    // bottom: "50%",
    // left: "17%",
    // paddingTop: "unset",
    // backgroundColor: "#00000070",
    // padding: "0px 11px",
    // borderRadius: "20%",
    // textAlign: "center",
  },
  icon: {
    marginBottom: "-2px",
    marginRight: "3px",
  },
  cardRelation: {
    padding: "initial",
  },
  details: {
    display: "flex",
    padding: "1px",
  },
  iconDetails: {
    position: "relative",
    bottom: "-3px",
    left: "-5px",
  },
  favIcon: {
    position: "absolute",
    right: "5px",
    top: "10px",
  },
  rateLabel: {
    position: "relative",
    top: "-5px",
  },
  marginAction: {
    marginLeft: "0px !important",
  },
  cardAction: { position: "absolute", width: "100%", bottom: "6px" },
}));

export default useStyles;
