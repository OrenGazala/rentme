import React from "react";
import { useSelector } from "react-redux";
import RentCard from "components/layout/rents/posts/card";
import useStyles from "./styles";
import { Grid, Container, Typography, Link } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import BackdropCircularLoading from "components/shared/backdrop-circular-loading";
import { APARTMENT_FROM } from "consts/route-consts";

const RentPosts = () => {
  const classes = useStyles();
  const { uid } = useSelector((state) => state.firebase.auth);
  const apartments = useSelector((state) => state.apartments.data);
  const loading = useSelector((state) => state.apartments.loading);

  if (loading) return <BackdropCircularLoading />;

  return (
    <Container className={classes.cardGrid} maxWidth="lg">
      <Grid container spacing={4}>
        {Object.keys(apartments).map((id) => (
          <RentCard key={id} id={id} from={APARTMENT_FROM.HOME} />
        ))}
      </Grid>
      {uid ? (
        <Typography variant="h5" align="center" style={{ marginTop: "24px" }}>
          <Link component={RouterLink} to={uid ? "/add-renter-details" : "/signin"}>
            Want better results? Let us get to know you better!
          </Link>
        </Typography>
      ) : undefined}
    </Container>
  );
};

export default RentPosts;
