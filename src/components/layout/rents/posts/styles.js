import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  sadEmoji: {
    position: "relative",
    top: "7px",
    left: "7px",
  },
}));

export default useStyles;
