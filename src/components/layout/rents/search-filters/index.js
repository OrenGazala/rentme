import React, { useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Grid,
  Typography,
  Container,
  Slider,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  TextField,
  Box,
} from "@material-ui/core";
import { SearchRounded, Remove } from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { propertyTypes } from "consts/shared-renter-aparment";
import useFunctions from "hooks/firebase/useFunctions";
import { getApartmentsAction, onFetchApartments } from "../../../../store/actions/apartments-actions";
import {
  GET_APARTMENTS_BY_FILTER,
  GET_APARTMENTS_BY_RENTER_PROPS,
  GET_APARTMENTS_BY_RANDOM,
} from "consts/firebase-functions";
import { bedroomsMarks } from "consts/shared-renter-aparment";
import { priceMarks } from "consts/shared-renter-aparment";
import useStyles from "./styles";
import useCityApi from "hooks/api/useCityApi";
import { debounce } from "lodash";
import useAlertDialog from "../../../../hooks/dialogs/useAlertDialog";

const initialFilterValues = {
  region: "",
  propertyType: "",
  bedrooms: [0.5, 1],
  price: 0,
};

const SearchFilters = () => {
  const classes = useStyles();
  const [filterValues, setFilterValues] = useState(initialFilterValues);
  const [advanced, setAdvanced] = useState(false);
  const functions = useFunctions();
  const dispatch = useDispatch();
  const [alertData, setAlertData] = useState({ type: "", message: "", title: "" });
  const showAlertDialog = useAlertDialog(alertData);

  const { uid } = useSelector((state) => state.firebase.auth);
  const { profile } = useSelector((state) => state.firebase);

  const handleChangeBedrooms = (event, newValue) => {
    setFilterValues({ ...filterValues, bedrooms: newValue });
  };

  const handleChangePrice = (event, newValue) => {
    setFilterValues({ ...filterValues, price: newValue });
  };

  const displayText = (value) => {
    return `$${value}`;
  };

  const search = () => {
    if (filterValues.region === "" || filterValues.propertyType === "" || filterValues.price === 0) {
      setAlertData({
        title: "Please enter all the required fields",
      });
      showAlertDialog();
    } else {
      dispatch(onFetchApartments(true));
      functions.call(GET_APARTMENTS_BY_FILTER, filterValues).then((res) => {
        dispatch(getApartmentsAction(res, true));
      });
    }
  };

  const hasRenterProps = () =>
    uid &&
    profile.renterProps.city.id &&
    profile.renterProps.propertyType &&
    profile.renterProps.bedrooms;

  const clearSearch = () => {
    if (filterValues.region !== "" || filterValues.propertyType !== "" || filterValues.price !== 0) {
      dispatch(onFetchApartments(true));
      if (hasRenterProps()) {
        functions.call(GET_APARTMENTS_BY_RENTER_PROPS).then((res) => dispatch(getApartmentsAction(res)));
      } else {
        functions.call(GET_APARTMENTS_BY_RANDOM).then((res) => dispatch(getApartmentsAction(res)));
      }
    }
    setFilterValues(initialFilterValues);
  };

  const cityApi = useCityApi();
  const [regionsApi, setRegionsApi] = useState([]);

  const handleRegionChange = (e, value) => {
    let region;
    if (regionsApi) {
      region = regionsApi.filter((region) => region.name === value)[0];
      if (region)
        setFilterValues({
          ...filterValues,
          region: region.name,
        });
    }
  };
  const handleInput = (e, prefix) => {
    if (prefix.length > 0)
      cityApi.getRegionsByPrefix(prefix).then((res) => {
        setRegionsApi(res);
      });
    else setRegionsApi([]);
  };

  const handler = useCallback(debounce(handleInput, 1000), []);

  const getRegionsLabels = () => {
    const labels = [];
    if (regionsApi && regionsApi.length > 0) regionsApi.map((option) => labels.push(option.name));
    return labels;
  };

  return (
    <div className={classes.heroContent}>
      <Container fixed maxWidth="md">
        <Typography
          component="h1"
          variant={window.innerWidth > 576 ? "h1" : "h3"}
          align="center"
          color="textPrimary"
          gutterBottom
        >
          Rent Me?
        </Typography>
        <div className={classes.heroButtons}>
          <ExpansionPanel className={classes.expansionPanel}>
            <ExpansionPanelSummary
              aria-label="Expand"
              aria-controls="additional-search-filter"
              id="advanced-search-filter"
              onClick={() => {
                setAdvanced(!advanced);
              }}
            >
              <Button variant="outlined" color="secondary" className={classes.buttonSearch}>
                {advanced ? (
                  <Remove fontSize="large" className={classes.searchStyle} />
                ) : (
                  <SearchRounded fontSize="large" className={classes.searchStyle} />
                )}
                <Box>Search for specific apartments you want to see</Box>
              </Button>
            </ExpansionPanelSummary>

            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              <Grid container spacing={4} alignItems="center" direction="row" justify="center">
                <Grid item xs={12} md={4}>
                  <Autocomplete
                    id="region-auto"
                    options={getRegionsLabels()}
                    value={filterValues.region ? filterValues.region : ""}
                    onInputChange={handler}
                    onChange={handleRegionChange}
                    // style={{ width: 230 }}
                    getOptionLabel={(option) => option}
                    renderInput={(params) => (
                      <TextField {...params} label="Region" variant="outlined" required />
                    )}
                  />
                </Grid>

                <Grid item xs={10} md={6} className={classes.root}>
                  <Typography id="range-bedrooms" gutterBottom align="center">
                    Bedrooms
                  </Typography>
                  <Slider
                    value={filterValues.bedrooms}
                    onChange={handleChangeBedrooms}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-bedrooms"
                    // style={{ width: 300 }} //140
                    min={0.5}
                    max={10}
                    step={0.5}
                    marks={bedroomsMarks}
                    valueLabelFormat={(value) => value}
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <Autocomplete
                    id="propertyType-auto"
                    options={Object.values(propertyTypes)}
                    inputValue={filterValues.propertyType}
                    onInputChange={(e, value) => {
                      setFilterValues({ ...filterValues, propertyType: value });
                    }}
                    getOptionLabel={(option) => option}
                    // style={{ width: 230 }}
                    renderInput={(params) => (
                      <TextField {...params} label="Property Type" variant="outlined" required />
                    )}
                  />
                </Grid>

                <Grid item xs={10} md={6} className={classes.root}>
                  <Typography id="range-price" gutterBottom align="center">
                    Price *
                  </Typography>
                  <Slider
                    value={filterValues.price}
                    onChange={handleChangePrice}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-price"
                    // style={{ width: 300 }} //140
                    min={0}
                    max={9999}
                    step={50}
                    marks={priceMarks}
                    valueLabelFormat={displayText}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={clearSearch}
                    style={{ width: 100 }}
                  >
                    Clear
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={search}
                    style={{ width: 140, marginLeft: "20px" }}
                  >
                    Search
                  </Button>
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      </Container>
    </div>
  );
};

export default SearchFilters;
