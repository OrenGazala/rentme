import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
    // borderStyle: "solid",
    justifyContent: "center",
    textAlign: "center",
    padding: theme.spacing(3, 1),
    // backgroundColor: "#606060",
  },
  expansionPanel: {
    display: "contents",
  },
  buttonSearch: {
    justifyContent: "center",
    width: "100%",
    borderRadius: "50px",
    textTransform: "capitalize",
  },
  expansionPanelDetails: {
    borderStyle: "solid",
    borderColor: "#fa4659",
    borderRadius: "16px",
    backgroundColor: "#606060",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
  },
  searchStyle: {
    padding: "0px 0px 0px 0px",
  },
  priceRoot: {
    width: 200,
  },
}));

export default useStyles;
