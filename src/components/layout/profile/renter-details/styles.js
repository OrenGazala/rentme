import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  bigAvatar: {
    margin: 10,
    width: 200,
    height: 200,
    fontSize: 100,
  },
  fixIcon: {
    marginBottom: "-2px",
  },
}));

export default useStyles;
