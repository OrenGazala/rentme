import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Typography, Button, Checkbox, Link } from "@material-ui/core";
import {
  Home,
  Room,
  AttachMoney,
  Pets,
  SmokingRooms,
  LocalHotelRounded,
  School,
  Work,
  Payment,
  Favorite,
} from "@material-ui/icons";
import { Link as RouterLink } from "react-router-dom";

const RenterDetails = ({ id }) => {
  const { uid } = useSelector((state) => state.firebase.auth);
  const history = useHistory();

  const renterProps = useSelector(
    (state) => state.firestore.data.users && state.firestore.data.users[id].renterProps
  );
  const renterDetailsFilled = renterProps.city.name && renterProps.propertyType && renterProps.bedrooms;

  return (
    <React.Fragment>
      {renterDetailsFilled ? (
        <div>
          <Typography color="textSecondary" variant="body1" gutterBottom>
            <Room color="primary" /> Looking to rent in {renterProps.city.region},{" "}
            {renterProps.city.name}
          </Typography>
          <Typography color="textSecondary" variant="body1" gutterBottom>
            <Home color="primary" /> Looking for {renterProps.propertyType}
          </Typography>
          <Typography color="textSecondary" variant="body1" gutterBottom>
            <AttachMoney color="primary" /> Willing to pay up to {renterProps.priceLimit}
          </Typography>
          <Typography color="textSecondary" variant="body1" gutterBottom>
            <LocalHotelRounded color="primary" /> Looking for {renterProps.bedrooms} bedrooms
          </Typography>
          <Typography hidden={!renterProps.salary} color="textSecondary" variant="body1" gutterBottom>
            <Payment color="primary" /> Earning {renterProps.salary} a month
          </Typography>
          <Typography hidden={!renterProps.education} color="textSecondary" variant="body1" gutterBottom>
            <School color="primary" /> {renterProps.education}
          </Typography>
          <Typography hidden={!renterProps.job} color="textSecondary" variant="body1" gutterBottom>
            <Work color="primary" /> {renterProps.job}
          </Typography>
          <Typography
            hidden={!renterProps.familyStatus}
            color="textSecondary"
            variant="body1"
            gutterBottom
          >
            <Favorite color="primary" /> {renterProps.familyStatus}
          </Typography>
          <Typography color="textSecondary" variant="body1" gutterBottom>
            <SmokingRooms color="primary" />
            <Checkbox
              disabled
              checked={renterProps.allowSmoke}
              name="smoker"
              color="primary"
              size="small"
            />
            <Pets color="primary" />
            <Checkbox disabled checked={renterProps.allowPet} name="pet" color="primary" size="small" />
          </Typography>
          {id === uid ? (
            <Typography style={{ display: "flex" }}>
              <Button
                variant="contained"
                style={{ marginLeft: "auto", float: "right", marginTop: "32px" }}
                onClick={() => history.push(`/add-renter-details`)}
              >
                Edit Renter Details
              </Button>
            </Typography>
          ) : (
            <></>
          )}
        </div>
      ) : id === uid ? (
        <div>
          <Typography variant="h4" align="center" style={{ marginTop: "24px" }}>
            <Link component={RouterLink} to={uid ? "/add-renter-details" : "/signin"}>
              Let us get to know you better!
            </Link>
          </Typography>
        </div>
      ) : (
        <></>
      )}
    </React.Fragment>
  );
};

export default RenterDetails;
