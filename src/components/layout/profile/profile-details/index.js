import React, { useState, useRef } from "react";
import { useSelector } from "react-redux";
import useCollection from "hooks/firebase/useCollection";
import useStorage from "hooks/firebase/useStorage";
import { USERS_COLLECTION } from "consts/collections";
import { Typography, Button, IconButton, Avatar, Container } from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import PermIdentityRounded from "@material-ui/icons/PermIdentityRounded";
import CakeIcon from "@material-ui/icons/Cake";
import EmailOutlined from "@material-ui/icons/EmailOutlined";
import Phone from "@material-ui/icons/Phone";
import { Edit } from "@material-ui/icons";
import useStyles from "./styles";
import EditDetailsDialog from "components/shared/dialogs/edit-details";

const ProfileDetails = ({ id }) => {
  const db = useCollection();
  const classes = useStyles();
  const storage = useStorage();

  const { uid } = useSelector((state) => state.firebase.auth);
  const profile = useSelector((state) => state.firestore.data.users && state.firestore.data.users[id]);

  const [open, setOpen] = useState(false);
  const [hover, sethover] = useState("hidden");

  const inputFile = useRef(null);
  const activeUser = id === uid;

  const chooseImage = async (e) => {
    if (e.target.files.length > 0) {
      const url = await storage.uploadFile(e.target.files[0], "user");
      if (url) {
        await db.update({
          collection: USERS_COLLECTION,
          doc: uid,
          data: { ...profile, profileImg: url },
        });
      }
    }
  };

  const onUpload = () => {
    inputFile.current.click();
  };

  const showImg = () => {
    if (profile.profileImg) return <Avatar className={classes.bigAvatar} src={profile.profileImg}></Avatar>;
    else return <Avatar className={classes.bigAvatar}>{profile.firstName[0]}</Avatar>;
  };

  return (
    <React.Fragment>
      <Container style={{ textAlign: "-webkit-center" }}>
        <Typography variant="h5" gutterBottom>
          <PermIdentityRounded className={classes.fixIcon} color="primary" /> {profile.firstName} {profile.lastName}
        </Typography>
        {activeUser ? (
          <div onMouseOver={() => sethover("visible")} onMouseOut={() => sethover("hidden")}>
            {showImg()}
            <div style={{ visibility: hover }}>
              <input type="file" id="file" ref={inputFile} style={{ display: "none" }} onChange={chooseImage} />
              <IconButton aria-label="edit" onClick={onUpload} className={classes.centered}>
                <Edit />
              </IconButton>
            </div>
          </div>
        ) : (
          <div>{showImg()} </div>
        )}
      </Container>
      <Typography color="textSecondary" variant="h6" gutterBottom>
        <Phone color="primary" className={classes.fixIcon} /> {profile.phoneNumber}
      </Typography>

      <Typography color="textSecondary" variant="h6" gutterBottom>
        <EmailOutlined color="primary" className={classes.fixIcon} /> {profile.email}
      </Typography>
      <Typography color="textSecondary" variant="h6" gutterBottom>
        <CakeIcon color="primary" className={classes.fixIcon} />{" "}
        {new Date(profile.birthDate.seconds * 1000).toLocaleDateString()}
      </Typography>
      <Typography>
        <Rating
          name="hover-feedback"
          value={Math.round(profile.feedbackDetails.avg * 2) / 2}
          precision={0.5}
          readOnly
        />
      </Typography>

      {activeUser ? (
        <Typography style={{ display: "flex" }}>
          <Button variant="contained" style={{ marginLeft: "auto", float: "right" }} onClick={() => setOpen(true)}>
            Edit Details
          </Button>
          <EditDetailsDialog open={open} onCancel={() => setOpen(false)}></EditDetailsDialog>
        </Typography>
      ) : (
        <></>
      )}
    </React.Fragment>
  );
};

export default ProfileDetails;
