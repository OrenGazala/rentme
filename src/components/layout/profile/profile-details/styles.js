import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  bigAvatar: {
    margin: "10px 0",
    width: 200,
    height: 200,
    fontSize: 100,
  },
  textField: {
    margin: theme.spacing(0, 0.5, 1),
    width: "110px",
  },
  numbersOnly: {
    "& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
      "-webkit-appearance": "none",
      margin: 0,
    },
    margin: theme.spacing(0, 0.5, 0),
  },
  centered: {
    marginTop: "-16rem",
  },
  fixIcon: {
    marginBottom: "-2px",
  },
}));

export default useStyles;
