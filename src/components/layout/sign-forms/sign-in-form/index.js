import React, { useState } from "react";
import useAuth from "hooks/firebase/useAuth";
import { Button, Link, TextField, Grid } from "@material-ui/core";

import useStyles from "./styles";
import { Link as RouterLink, useHistory } from "react-router-dom";

const SignInForm = () => {
  const initialCredentials = { email: "", password: "" };
  const [credentials, setCredentials] = useState(initialCredentials);
  const history = useHistory();
  const authService = useAuth();
  const classes = useStyles();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCredentials({
      ...credentials,
      [name]: value,
    });
  };

  const signInWithEmailAndPassword = async (e) => {
    e.preventDefault();

    const success = await authService.signInWithEmailAndPassword(
      credentials.email,
      credentials.password
    );
    if (success) history.push("/");
  };

  return (
    <form className={classes.form} onSubmit={signInWithEmailAndPassword}>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        label="Email Address"
        name="email"
        type="email"
        autoFocus
        value={credentials.email}
        onChange={handleChange}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        value={credentials.password}
        onChange={handleChange}
      />
      <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
        Sign In
      </Button>
      <Grid container>
        <Grid item>
          <Link component={RouterLink} to="/signup">
            Don't have an account? Sign Up
          </Link>
        </Grid>
      </Grid>
    </form>
  );
};

export default SignInForm;
