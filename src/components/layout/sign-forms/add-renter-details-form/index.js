import React, { useState, useCallback } from "react";
import {
  Grid,
  TextField,
  InputAdornment,
  Button,
  MenuItem,
  Typography,
  Slider,
  Chip,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from "@material-ui/core";

import {
  School,
  Work,
  AttachMoney,
  Hotel,
  Payment,
  HomeWork,
  Favorite,
  LocalParking,
  DirectionsBus,
  Height,
  Deck,
  AcUnit,
  Weekend,
  ChildCare,
  MenuBook,
  LocalBar,
  SmokingRooms,
  Pets,
  People,
  Done,
  ExpandMore,
  Bathtub,
} from "@material-ui/icons";

import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import useCollection from "hooks/firebase/useCollection";
import { USERS_COLLECTION } from "consts/collections";
import useStyles from "./styles";
import { EDUCATIONS, SALARIES, FAMILY_STATUSES } from "consts/renter-props";
import {
  priceMarks,
  partnersMarks,
  bathroomsMarks,
  bedroomsMarks,
  propertyTypes,
} from "consts/shared-renter-aparment";
import { isLoaded } from "react-redux-firebase";
import CircularLoading from "components/shared/circular-loading";
import { saveTabIndexAction } from "store/actions/ui-actions";
import { getApartmentsAction } from "store/actions/apartments-actions";
import useCityApi from "hooks/api/useCityApi";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { debounce } from "lodash";
import { GET_APARTMENTS_BY_RENTER_PROPS } from "consts/firebase-functions";
import useFunctions from "hooks/firebase/useFunctions";

const AddRenterDetailsForm = () => {
  const classes = useStyles();
  const history = useHistory();
  const db = useCollection();
  const cityApi = useCityApi();
  const dispatch = useDispatch();

  const functions = useFunctions();
  const { uid } = useSelector((state) => state.firebase.auth);
  const { profile } = useSelector((state) => state.firebase);
  const [renterProps, setRenter] = useState(profile.renterProps);
  const [citiesApi, setCities] = useState([]);

  const handleInput = (e, prefix) => {
    if (prefix.length > 1)
      cityApi.getCitiesByPrefix(prefix).then((res) => {
        setCities(res);
      });
  };

  const handler = useCallback(debounce(handleInput, 1000), []);

  const indicationsChips = [
    { name: "hasParking", header: "Parking", icon: <LocalParking /> },
    { name: "hasPublicTransport", header: "Public Transport", icon: <DirectionsBus /> },
    { name: "hasElevator", header: "Elevator", icon: <Height /> },
    { name: "hasBalcony", header: "Balcony", icon: <Deck /> },
    { name: "hasAirConditioning", header: "Air Conditioning", icon: <AcUnit /> },
    { name: "hasFurnitures", header: "Furnitures", icon: <Weekend /> },
    { name: "hasSchool", header: "School", icon: <School /> },
    { name: "hasKinderGardens", header: "Kinder Gardens", icon: <ChildCare /> },
    { name: "hasSynagogue", header: "Synagogue", icon: <MenuBook /> },
    { name: "hasBars", header: "Bars", icon: <LocalBar /> },
    { name: "allowSmoke", header: "Allow Smoke", icon: <SmokingRooms /> },
    { name: "allowPet", header: "Allow Pets", icon: <Pets /> },
  ];

  const renterTexts = [
    {
      name: "education",
      label: "Education",
      helperText: "Please select your education",
      icon: <School />,
      valuesArray: EDUCATIONS,
    },
    {
      name: "job",
      label: "Job",
      helperText: "What is your job?",
      icon: <Work />,
    },
    {
      name: "salary",
      label: "Salary",
      helperText: "How much do you earn?",
      icon: <Payment />,
      valuesArray: SALARIES,
    },

    {
      name: "familyStatus",
      label: "Family Status",
      helperText: "What is your family status?",
      icon: <Favorite />,
      valuesArray: FAMILY_STATUSES,
    },
  ];

  if (!isLoaded(profile)) {
    return <CircularLoading />;
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setRenter({
      ...renterProps,
      [name]: value,
    });
  };

  const handleCityChange = (e, value) => {
    if (citiesApi)
      setRenter({
        ...renterProps,
        city: { ...citiesApi.find((cityApi) => cityApi.region + " - " + cityApi.name === value) },
      });
  };

  const handlePriceChange = (event, newValue) => {
    setRenter({
      ...renterProps,
      priceLimit: newValue,
    });
  };

  const handleBedroomsChange = (event, newValue) => {
    setRenter({
      ...renterProps,
      bedrooms: newValue,
    });
  };

  const handleBathroomsChange = (event, newValue) => {
    setRenter({
      ...renterProps,
      bathrooms: newValue,
    });
  };

  const handlePartnersChange = (event, newValue) => {
    setRenter({
      ...renterProps,
      partners: newValue,
    });
  };

  const handleCheckboxChange = (e) => {
    const name = e.currentTarget.title ? e.currentTarget.title : e.currentTarget.parentElement.title;
    setRenter({
      ...renterProps,
      [name]: !renterProps[name],
    });
  };

  const signUpRenter = async (e) => {
    e.preventDefault();
    const success = await db.update({
      collection: USERS_COLLECTION,
      doc: uid,
      data: { renterProps },
    });
    if (success) {
      dispatch(saveTabIndexAction(0));
      functions.call(GET_APARTMENTS_BY_RENTER_PROPS).then((res) => dispatch(getApartmentsAction(res)));
      history.push("/");
    }
  };

  const getCitiesLabels = () => {
    const labels = [];
    if (citiesApi) citiesApi.map((option) => labels.push(option.region + " - " + option.name));
    return labels;
  };

  const apartmentSliders = [
    {
      name: "priceLimit",
      label: "Monthly Rent Limit",
      helperText: "What is the most expensive price you can pay each month?",
      icon: <AttachMoney />,
      onChange: handlePriceChange,
      min: 0,
      max: 9999,
      step: 50,
      marks: priceMarks,
    },
    {
      name: "partners",
      label: "Partners",
      helperText: "How many partners in the apartment?",
      icon: <People />,
      onChange: handlePartnersChange,
      min: 1,
      max: 10,
      step: 1,
      marks: partnersMarks,
    },
    {
      name: "bedrooms",
      label: "Bedrooms",
      helperText: "How many bedrooms in the apartment?",
      icon: <Hotel />,
      onChange: handleBedroomsChange,
      min: 0.5,
      max: 10,
      step: 0.5,
      marks: bedroomsMarks,
    },
    {
      name: "bathrooms",
      label: "Bathrooms",
      helperText: "How many bathrooms in the apartment?",
      icon: <Bathtub />,
      onChange: handleBathroomsChange,
      min: 0,
      max: 10,
      step: 0.5,
      marks: bathroomsMarks,
    },
  ];

  return (
    <form className={classes.form} onSubmit={signUpRenter}>
      <Grid container spacing={4}>
        <Grid item xs={12} className={classes.header}>
          <Typography variant="caption" display="block">
            Recommended, so we can find you suitable appartments
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Autocomplete
            options={getCitiesLabels()}
            value={renterProps.city.id ? `${renterProps.city.region} - ${renterProps.city.name}` : ""}
            onInputChange={handler}
            onChange={handleCityChange}
            getOptionLabel={(option) => option}
            renderInput={(params) => <TextField {...params} label="City" variant="outlined" required />}
          />
          <Typography variant="caption" gutterBottom align="center" color="textSecondary">
            Where are the cities you want to find apartments?
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            variant="outlined"
            fullWidth
            required
            select
            label="Property Type"
            name="propertyType"
            value={renterProps.propertyType}
            onChange={handleChange}
            helperText="What property type you want?"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <HomeWork />
                </InputAdornment>
              ),
            }}
          >
            {Object.values(propertyTypes).map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        {apartmentSliders.map((slider, index) => (
          <Grid container item xs={12} sm={6} spacing={0} key={index}>
            <Typography gutterBottom align="center">
              {slider.label}
            </Typography>
            <Grid container spacing={2}>
              <Grid item>{slider.icon}</Grid>
              <Grid item xs>
                <Slider
                  name={slider.name}
                  valueLabelDisplay="auto"
                  value={renterProps[slider.name]}
                  onChange={slider.onChange}
                  min={slider.min}
                  max={slider.max}
                  marks={slider.marks}
                  step={slider.step}
                  aria-labelledby="range-price"
                />
              </Grid>
            </Grid>
            <Typography variant="caption" gutterBottom align="center" color="textSecondary">
              {slider.helperText}
            </Typography>
          </Grid>
        ))}
        <Grid container item xs={false} sm={6} spacing={0} />
        <Grid item xs={12}>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMore />} aria-controls="panel1c-content">
              <div>Your details - The more you enter you will be graded higher</div>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={4}>
                {renterTexts.map((renterText, index) => (
                  <Grid item xs={12} sm={6} key={index}>
                    <TextField
                      variant="outlined"
                      select={renterText.valuesArray ? true : false}
                      fullWidth
                      label={renterText.label}
                      name={renterText.name}
                      value={renterProps[renterText.name] ? renterProps[renterText.name] : ""}
                      onChange={handleChange}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">{renterText.icon}</InputAdornment>
                        ),
                      }}
                      helperText={renterText.helperText}
                    >
                      {renterText.valuesArray
                        ? Object.values(renterText.valuesArray).map((option) => (
                            <MenuItem key={option} value={option}>
                              {option}
                            </MenuItem>
                          ))
                        : undefined}
                    </TextField>
                  </Grid>
                ))}
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid item xs={12}>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMore />} aria-controls="panel1c-content">
              <div>
                More apartment Details - The more you enter we will show better apartments for you
              </div>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={1}>
                {indicationsChips.map((chip, index) => (
                  <Grid item xs={12} sm={3} key={index}>
                    <Chip
                      variant={renterProps[chip.name] ? "default" : "outlined"}
                      color={renterProps[chip.name] ? "primary" : "default"}
                      className={classes.chip}
                      label={chip.header}
                      icon={chip.icon}
                      onClick={handleCheckboxChange}
                      deleteIcon={<Done />}
                      onDelete={renterProps[chip.name] ? handleCheckboxChange : undefined}
                      title={chip.name}
                    />
                  </Grid>
                ))}
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
            Save
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddRenterDetailsForm;
