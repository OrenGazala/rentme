import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  numbersOnly: {
    "& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
      "-webkit-appearance": "none",
      margin: 0,
    },
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  centered: {
    position: "absolute",
    marginTop: "-8rem",
    left: "50%",
    transform: "translate(-50%, -50%)",
    // backgroundColor: "gray",
    alignItems: "center",
    // boxSizing: "border-box",
    color: "rgba(var(--inverse-rgb), 0.8)",
    display: "flex",
    flexDirection: "column",
    fontSize: "13px",
    height: "fit-parent",
    justifyContent: "center",
    lineHeight: "15px",
    width: "fit-parent",
    zIndex: "1000",
  },
  large: {
    width: theme.spacing(30),
    height: theme.spacing(30),
    margin: "auto",
  },
  largeChip: {
    height: theme.spacing(8),
  },
  header: {
    textAlign: "center",
  },
}));

export default useStyles;
