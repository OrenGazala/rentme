import React, { useState, useRef } from "react";
import {
  Button,
  TextField,
  Link,
  Grid,
  Avatar,
  IconButton,
  InputAdornment,
  Typography,
  Chip,
} from "@material-ui/core";
import {
  AccountCircle,
  Email,
  VpnKey,
  Phone,
  Edit,
  PhotoCamera,
  Done,
  House,
  VerifiedUser,
} from "@material-ui/icons";
import DateFnsUtils from "@date-io/date-fns";

import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import { Link as RouterLink, useHistory } from "react-router-dom";

import useStorage from "hooks/firebase/useStorage";
import UserModel from "models/user";
import useStyles from "./styles";
import useAuth from "hooks/firebase/useAuth";

const SignUpForm = () => {
  const classes = useStyles();
  const history = useHistory();
  const authService = useAuth();
  const storage = useStorage();
  const inputFile = useRef(null);

  const [user, setUser] = useState(UserModel);
  const [isLandLord, setIsLandLord] = useState(false);
  const [profileImg, setProfileImg] = useState(user.profileImg);
  const [hover, sethover] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({
      ...user,
      [name]: value,
    });
  };

  const handleDateChange = (birthDate) => {
    setUser({
      ...user,
      birthDate,
    });
  };

  const signUpUser = async (e) => {
    e.preventDefault();
    user.profileImg = profileImg;
    const success = await authService.createUserWithEmailAndPassword(user);

    if (success) {
      if (!isLandLord) history.push("/add-renter-details");
      else history.push("/add-apartment");
    }
  };

  const chooseImage = async (e) => {
    if (e.target.files.length > 0) {
      const url = await storage.uploadFile(e.target.files[0], "user");
      if (url) setProfileImg(url);
    }
  };

  const onUpload = () => {
    inputFile.current.click();
  };

  return (
    <form className={classes.form} onSubmit={signUpUser}>
      <Grid container spacing={2}>
        <Grid item xs={12} className={classes.header}>
          {profileImg ? (
            <div onMouseOver={() => sethover(true)} onMouseOut={() => sethover(false)}>
              {hover ? (
                <div>
                  <Avatar src={profileImg} className={classes.large} />
                  <input
                    type="file"
                    id="file"
                    ref={inputFile}
                    style={{ display: "none" }}
                    onChange={chooseImage}
                  />
                  <IconButton onClick={onUpload} className={classes.centered}>
                    <Avatar>
                      <Edit />
                    </Avatar>
                  </IconButton>
                </div>
              ) : (
                <Avatar src={profileImg} className={classes.large} />
              )}
            </div>
          ) : (
            <div>
              <Typography variant="caption" display="block" color="textSecondary">
                Add Profile Picture
              </Typography>
              <input
                type="file"
                id="file"
                ref={inputFile}
                style={{ display: "none" }}
                onChange={chooseImage}
              />
              <IconButton onClick={onUpload}>
                <Avatar className={classes.avatar}>
                  <PhotoCamera />
                </Avatar>
              </IconButton>
            </div>
          )}
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            variant="outlined"
            required
            fullWidth
            label="First Name"
            name="firstName"
            autoFocus
            value={user.firstName}
            onChange={handleChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            variant="outlined"
            required
            fullWidth
            label="Last Name"
            name="lastName"
            value={user.lastName}
            onChange={handleChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="email"
            variant="outlined"
            required
            fullWidth
            label="Email"
            type="email"
            value={user.email}
            onChange={handleChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Email />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="password"
            variant="outlined"
            required
            fullWidth
            label="Password"
            type="password"
            value={user.password}
            onChange={handleChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VpnKey />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            variant="outlined"
            required
            fullWidth
            label="Phone Number"
            name="phoneNumber"
            className={classes.numbersOnly}
            value={user.phoneNumber}
            onChange={handleChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Phone />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            variant="outlined"
            required
            fullWidth
            label="ID Number"
            name="IDNumber"
            type="number"
            className={classes.numbersOnly}
            value={user.IDNumber}
            onChange={handleChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VerifiedUser />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              inputVariant="outlined"
              format="dd/MM/yyyy"
              margin="normal"
              label="Birth Date"
              value={user.birthDate}
              onChange={handleDateChange}
              maxDate={new Date().setFullYear(new Date().getFullYear() - 18)}
              maxDateMessage="Must be 18 or older"
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
            />
          </MuiPickersUtilsProvider>
        </Grid>
        <Grid item xs={12}>
          <Chip
            variant={isLandLord ? "default" : "outlined"}
            color={isLandLord ? "primary" : "default"}
            className={classes.largeChip}
            label={isLandLord ? "I own an apartment" : "Are you an apartment's owner?"}
            icon={<House />}
            onClick={(e) => setIsLandLord(!isLandLord)}
            deleteIcon={<Done />}
            onDelete={isLandLord ? (e) => setIsLandLord(!isLandLord) : undefined}
          />
        </Grid>
      </Grid>
      <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
        Sign Up
      </Button>
      <Grid container justify="flex-start">
        <Grid item>
          <Link component={RouterLink} to="/signin" variant="body2">
            Already have an account? Sign in
          </Link>
        </Grid>
      </Grid>
    </form>
  );
};

export default SignUpForm;
