import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  addFeedback: {
    margin: "10px 0px",
    textAlign: "center",
    justifyContent: "center",
    padding: "inherit",
    // backgroundColor: "#5edfff",
    border: "1px solid whitesmoke",
    borderRadius: theme.spacing(3),
  },
  feedback: {
    width: "80%",
    marginRight: "3px",
  },
  add: {
    marginTop: theme.spacing(2),
  },
  rating: {
    margin: theme.spacing(2, 2, 0, 0),
  },
  ratingText: {
    marginTop: "20px",
  },
}));

export default useStyles;
