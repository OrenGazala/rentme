import React, { useState } from "react";
import useCollection from "hooks/firebase/useCollection";
import useStyles from "./styles";
import Rating from "@material-ui/lab/Rating";
import { useSelector } from "react-redux";
import { FEEDBACKS_COLLECTION, USERS_COLLECTION } from "consts/collections";
import { TextField, Button, Grid, Box } from "@material-ui/core";
import FeedbackModel from "models/feedback";
import { labels } from "consts/feedbacks-labels";
import { getRentsByUIDAndApartmentID } from "queries/rents-queries";
import { useFirestoreConnect, isEmpty } from "react-redux-firebase";

const AddFeedback = ({ collection, uid, apartmentID, onAdd }) => {
  const classes = useStyles();
  const db = useCollection();

  useFirestoreConnect(getRentsByUIDAndApartmentID(uid, apartmentID));
  const rents = useSelector((state) => state.firestore.data.rents);

  const [feedback, setFeedback] = useState({ ...FeedbackModel, uid });
  const [hover, setHover] = React.useState(-1);

  const addFeedback = async () => {
    if (feedback.content || feedback.rating) {
      const doc = collection === USERS_COLLECTION ? uid : apartmentID;
      const success = await db.addToSubCollection({
        collection,
        doc: doc,
        subCollection: FEEDBACKS_COLLECTION,
        data: feedback,
      });
      if (success) {
        setFeedback({ ...FeedbackModel, uid });
        if (onAdd) onAdd();
      }
    }
  };

  if (isEmpty(rents)) return <></>;

  return (
    <Grid container className={classes.addFeedback}>
      <Grid item xs={12} md={3} lg={3} style={{ display: "flex" }}>
        <Rating
          name="simple-controlled"
          className={classes.rating}
          value={feedback.rating}
          precision={0.5}
          onChange={(_, rating) => setFeedback({ ...feedback, rating })}
          onChangeActive={(event, newHover) => {
            setHover(newHover);
          }}
        />
        {feedback.rating !== null && (
          <Box className={classes.ratingText} ml={1}>
            {labels[hover !== -1 ? hover : feedback.rating]}
          </Box>
        )}
      </Grid>
      <Grid item xs={12} md={8} lg={8}>
        <TextField
          label="Add a Feedback"
          value={feedback.content}
          onChange={(e) => setFeedback({ ...feedback, content: e.target.value })}
          multiline
          className={classes.feedback}
        />
        <Button onClick={addFeedback} className={classes.add} color="primary">
          Add
        </Button>
      </Grid>
    </Grid>
  );
};

export default AddFeedback;
