import React from "react";
import useStyles from "./styles";
import CircularLoading from "components/shared/circular-loading";
import { List } from "@material-ui/core";
import { useSelector } from "react-redux";
import { useFirestoreConnect, isLoaded, isEmpty } from "react-redux-firebase";
import { getFeedbacks } from "queries/feedbacks";
import FeedbackDetails from "../details";

const ShowFeedbacks = ({ collection, doc }) => {
  const classes = useStyles();

  useFirestoreConnect(getFeedbacks(collection, doc));
  const feedbacks = useSelector((state) => state.firestore.data.feedbacks);

  if (!isLoaded(feedbacks)) return <CircularLoading />;
  if (isEmpty(feedbacks)) return <></>;

  return (
    <List className={classes.root}>
      {Object.keys(feedbacks).map((id) => (
        <FeedbackDetails key={id} id={id} />
      ))}
    </List>
  );
};

export default ShowFeedbacks;
