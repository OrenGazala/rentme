import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: "0px 0px",
    width: "100%",
    padding: theme.spacing(1),
  },
}));

export default useStyles;
