import React from "react";
import { useSelector } from "react-redux";
import { ListItem, Divider, ListItemText, Avatar, ListItemAvatar } from "@material-ui/core";
import useStyles from "./styles";
import Rating from "@material-ui/lab/Rating";

const FeedbackDetails = ({ id }) => {
  const classes = useStyles();
  const feedback = useSelector((state) => state.firestore.data.feedbacks[id]);

  return (
    <ListItem className={classes.listItem}>
      <ListItemAvatar>
        <Avatar
          classes={{ colorDefault: classes.avatarImage }}
          alt="Remy Sharp"
          src="/static/images/avatar/1.jpg"
        />
      </ListItemAvatar>

      <ListItemText
        classes={{ multiline: classes.feedbackContent }}
        primary={
          <div>
            <Rating value={feedback.rating} precision={0.5} readOnly />
            <br />
            {feedback.content}
          </div>
        }
        // convert unix timestamp from firestore to date. workaround for redux persist
        secondary={new Date(feedback.startDate.seconds * 1000).toLocaleString()}
      />
      <Divider orientation="horizontal" flexItem />
    </ListItem>
  );
};

export default FeedbackDetails;
