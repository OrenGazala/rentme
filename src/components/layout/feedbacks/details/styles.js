import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  listItem: {
    backgroundColor: "#97979740",
    border: "1px solid white",
    borderRadius: theme.spacing(3),
    margin: theme.spacing(2, 0),
  },
  avatarImage: {
    margin: "auto",
  },
  feedbackContent: {
    marginLeft: theme.spacing(2),
  },
}));

export default useStyles;
