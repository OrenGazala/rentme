import React from "react";
import useCollection from "hooks/firebase/useCollection";
import { IconButton, Tooltip, Zoom, CardActions } from "@material-ui/core";
import { Favorite, FavoriteBorderRounded } from "@material-ui/icons";
import { APARTMENT_IMPRESSIONS_COLLECTION } from "consts/collections";
import { updateLikeApartmentAction } from "store/actions/apartments-actions";
import { useDispatch, useSelector } from "react-redux";

const Like = ({ uid, apartmentID, positionStyle }) => {
  const impressionObject = useSelector(
    (state) =>
      state.apartments.likedApartments &&
      state.apartments.likedApartments[apartmentID] &&
      state.apartments.likedApartments[apartmentID].impression
  );

  let doc, impression;
  if (impressionObject) {
    doc = impressionObject.key;
    impression = impressionObject.value;
  }

  const db = useCollection();
  const dispatch = useDispatch();

  const handleLikeChange = async (like) => {
    if (impression) {
      await db.update({
        collection: APARTMENT_IMPRESSIONS_COLLECTION,
        doc,
        data: { liked: !impression.liked },
      });
      dispatch(
        updateLikeApartmentAction({
          apartmentID,
          impression: {
            key: doc,
            value: { ...impression, liked: !impression.liked },
          },
        })
      );
    } else {
      const data = { apartmentID, uid, liked: true };
      const ref = await db.add({
        collection: APARTMENT_IMPRESSIONS_COLLECTION,
        data,
      });
      dispatch(
        updateLikeApartmentAction({
          apartmentID,
          impression: {
            key: ref.id,
            value: data,
          },
        })
      );
    }
  };

  return (
    <CardActions style={{ justifyContent: "center", position: positionStyle, left: "8px" }}>
      <Tooltip title="Like" TransitionComponent={Zoom} arrow>
        <IconButton
          aria-label="like"
          onClick={() => handleLikeChange()}
          style={{
            boxShadow: "0px 0px 5px 0px",
            backgroundColor: "white",
            color: "black",
          }}
        >
          {impression && impression.liked ? (
            <Favorite style={{ color: "#ff1c1ca1" }} fontSize="large" />
          ) : (
            <FavoriteBorderRounded fontSize="large" />
          )}
        </IconButton>
      </Tooltip>
    </CardActions>
  );
};

export default Like;
