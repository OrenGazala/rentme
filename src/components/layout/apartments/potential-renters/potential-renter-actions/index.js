import React, { useState } from "react";
import { Button, ExpansionPanelActions, Tooltip } from "@material-ui/core";
import { Delete, PersonAdd, PersonAddDisabled, Person } from "@material-ui/icons";
import { useHistory, useParams } from "react-router-dom";
import useCollection from "hooks/firebase/useCollection";
import {
  APARTMENT_IMPRESSIONS_COLLECTION,
  RENTS_COLLECTION,
  APARTMENTS_COLLECTION,
} from "consts/collections";
import { useDispatch, useSelector } from "react-redux";
import { deleteRent, addRent, deleteImpression } from "store/actions/potential-renters-actions";
import PotentialRenterFeedback from "components/shared/dialogs/potential-renter-feedback";
import swal from "sweetalert";

const PotentialRenterActions = ({ isRenter, id, uid, disableEntry }) => {
  const history = useHistory();
  const db = useCollection();
  const dispatch = useDispatch();
  const { apartmentID } = useParams();

  const apartment = useSelector((state) => state.potentialRenters.data);
  const enterUser = Object.values(apartment.impressions).find((impression) => impression.uid === uid);

  const [open, setOpen] = useState(false);

  const addToRents = async () => {
    const success = await db.add({
      collection: RENTS_COLLECTION,
      data: { uid, apartmentID },
    });
    if (success) {
      if (disableEntry === 1) {
        await deactivatePost();
        swal("You filled the Apartment!", "The post is now unactivated", "success");
      }
      dispatch(addRent(success.id, uid, enterUser.user));
      await removeFromImpresstions();
    }
  };

  const deactivatePost = async () => {
    await db.update({
      collection: APARTMENTS_COLLECTION,
      doc: apartmentID,
      data: { active: false },
    });
  };

  const removeFromRents = async () => {
    const success = await db.remove({ collection: RENTS_COLLECTION, doc: id });
    if (success) {
      dispatch(deleteRent(id));
    }
  };

  const handleClose = () => {
    setOpen(false);
    removeFromRents();
  };

  const removeFromImpresstions = async () => {
    const success = await db.remove({ collection: APARTMENT_IMPRESSIONS_COLLECTION, doc: id });
    if (success) dispatch(deleteImpression(id));
  };

  const renterButtons = () => {
    return (
      <>
        <Tooltip disableFocusListener title="Delete Potential renter">
          <Button
            startIcon={<Delete />}
            color="secondary"
            variant="outlined"
            size="small"
            onClick={removeFromImpresstions}
          >
            Delete
          </Button>
        </Tooltip>
        <Tooltip
          disableFocusListener
          title={
            disableEntry
              ? "Add Potential renter to the apartment"
              : "You don't have avilable space in the apartment"
          }
        >
          <span>
            <Button
              size="small"
              color="primary"
              disabled={!disableEntry}
              startIcon={<PersonAdd />}
              variant="outlined"
              onClick={addToRents}
            >
              Add
            </Button>
          </span>
        </Tooltip>
      </>
    );
  };

  const PotentialRenterButtons = () => {
    return (
      <>
        <Tooltip disableFocusListener title="Remove Renter from the apartment">
          <Button
            size="small"
            color="secondary"
            startIcon={<PersonAddDisabled />}
            variant="outlined"
            onClick={() => setOpen(true)}
          >
            Remove
          </Button>
        </Tooltip>
        <PotentialRenterFeedback
          uid={uid}
          apartmentID={apartmentID}
          open={open}
          handleClose={handleClose}
        />
      </>
    );
  };

  return (
    <ExpansionPanelActions>
      <Tooltip disableFocusListener title="Go to user profile">
        <Button
          size="small"
          startIcon={<Person />}
          color="primary"
          onClick={() => history.push(`/user-profile/${uid}`)}
          variant="outlined"
        >
          Profile
        </Button>
      </Tooltip>
      {!isRenter ? renterButtons() : PotentialRenterButtons()}
    </ExpansionPanelActions>
  );
};

export default PotentialRenterActions;
