import { makeStyles } from "@material-ui/core/styles";
import { green, yellow, red } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  root: {
    // width: "50%",
    // marginLeft: "25%",
    marginBottom: "17%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: "bottom",
    height: 20,
    width: 20,
  },
  details: {
    alignItems: "center",
  },
  column: {
    marginRight: "5px",
  },
  chipColumn: {
    flexBasis: "100%",
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: theme.spacing(1, 2),
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: "none",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  grade: {
    fontSize: "12px",
  },
  greatGrade: {
    color: green["A700"],
    borderColor: green["A700"],
  },
  goodGrade: {
    color: yellow[700],
    borderColor: yellow[700],
  },
  averageGrade: {
    color: red["A700"],
    borderColor: red["A700"],
  },
  chip: {
    marginTop: theme.spacing(2),
  },
  chips: {
    // width: "50%",
  },
  gridChip: {
    display: "flex",
    justifyContent: "center",
  },
  name: {
    marginTop: "7px",
    marginLeft: "5px",
  },
  checkbox: {
    width: "10%",
  },
  checkboxLabel: {
    margin: "0",
  },
  impressionsTitle: {
    textAlign: "center",
    marginTop: "20px",
    marginBottom: "20px",
  },
  rentersTitle: {
    textAlign: "center",
    marginTop: "20px",
    marginBottom: "20px",
  },
}));
export default useStyles;
