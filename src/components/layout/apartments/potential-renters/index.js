import React from "react";
import { Typography } from "@material-ui/core";
import useStyles from "./styles";
import { useSelector } from "react-redux";
import PotentialRenter from "./potential-renter";

const PotentialRenters = () => {
  const classes = useStyles();
  const apartment = useSelector((state) => state.potentialRenters.data);

  const rentsLength = Object.keys(apartment.rents).length;
  const impressionsLength = Object.keys(apartment.impressions).length;
  const disableEntry = apartment.partners - rentsLength;

  if (!rentsLength && !impressionsLength) {
    return (
      <Typography component="h5" variant="h5" align="center" color="textPrimary" gutterBottom>
        You dont have any potential renters
      </Typography>
    );
  }

  return (
    <div className={classes.root}>
      <Typography variant="h4">{apartment.address}</Typography>
      <Typography variant="subtitle2">
        {rentsLength}/{apartment.partners} People in your apartment
      </Typography>
      {rentsLength ? (
        <Typography variant="h6" className={classes.rentersTitle}>
          Renters in the apartment
        </Typography>
      ) : (
        <></>
      )}
      {Object.keys(apartment.rents).map((id) => (
        <PotentialRenter key={id} id={id} isRenter={true} disableEntry={disableEntry} />
      ))}
      {impressionsLength ? (
        <Typography variant="h6" className={classes.impressionsTitle}>
          The apartment's Potential renters
        </Typography>
      ) : (
        <></>
      )}
      {Object.keys(apartment.impressions).map((id) => (
        <PotentialRenter key={id} id={id} isRenter={false} disableEntry={disableEntry} />
      ))}
    </div>
  );
};

export default PotentialRenters;
