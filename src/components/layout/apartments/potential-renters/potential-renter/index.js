import React from "react";
import {
  Grid,
  Typography,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Divider,
  Chip,
  Avatar,
} from "@material-ui/core";
import {
  Pets,
  ChildCare,
  Work,
  School,
  SmokingRooms,
  ChildFriendly,
  AttachMoney,
} from "@material-ui/icons";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import useStyles from "../styles";
import PotentialRenterActions from "../potential-renter-actions";
import { useSelector } from "react-redux";

const PotentialRenter = ({ id, isRenter, disableEntry }) => {
  const classes = useStyles();

  const { uid, user } = useSelector((state) =>
    isRenter ? state.potentialRenters.data.rents[id] : state.potentialRenters.data.impressions[id]
  );

  const renterHasPet = (hasPet) => (!hasPet ? "Doesn't have a pet" : "Has a Pet");
  const renterSmoker = (smoker) => (!smoker ? "Doesn't smoke" : "Smokes");
  const renterAge = (birthDate) => {
    const ageDate = new Date(Date.now() - birthDate._seconds * 1000);
    const age = Math.abs(ageDate.getUTCFullYear() - 1970);
    return "Age: " + age;
  };
  const gradeColor = (grade) => {
    if (grade >= 8) return classes.greatGrade;
    if (grade >= 5) return classes.goodGrade;
    return classes.averageGrade;
  };

  const indicationsChips = [
    { label: renterAge(user.birthDate), icon: <ChildCare /> },
    { label: user.renterProps.job, icon: <Work /> },
    { label: user.renterProps.education, icon: <School /> },
    { label: user.renterProps.salary, icon: <AttachMoney /> },
    { label: user.renterProps.familyStatus, icon: <ChildFriendly /> },
    { label: renterHasPet(user.renterProps.allowPet), icon: <Pets /> },
    { label: renterSmoker(user.renterProps.allowSmoke), icon: <SmokingRooms /> },
  ];

  return (
    <ExpansionPanel key={user}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1c-content">
        {user.agreementLevel ? (
          <div className={classes.column}>
            <Avatar
              className={`${classes.grade} ${gradeColor(user.agreementLevel)}`}
              style={{
                border: "1px solid",
                fontSize: "1.3rem",
                fontWeight: "bold",
                backgroundColor: "white",
              }}
            >
              {user.agreementLevel}
            </Avatar>
          </div>
        ) : (
          <></>
        )}
        <Avatar className={classes.heading} src={user.profileImg} />
        <div className={classes.name}>
          <Typography className={classes.heading}>
            {user.firstName} {isRenter ? " Is in the Apartment!" : ""}
          </Typography>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.details}>
        <Grid container className={classes.chips}>
          {indicationsChips.map((chip, index) =>
            chip.label ? (
              <Grid item xs={12} lg={2} key={index} className={classes.gridChip}>
                <Chip icon={chip.icon} className={classes.chip} label={chip.label} color="primary" />
              </Grid>
            ) : (
              <></>
            )
          )}
        </Grid>
      </ExpansionPanelDetails>
      <Divider />
      <PotentialRenterActions id={id} isRenter={isRenter} uid={uid} disableEntry={disableEntry} />
    </ExpansionPanel>
  );
};

export default PotentialRenter;
