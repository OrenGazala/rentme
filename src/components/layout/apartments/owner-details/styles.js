import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  rootActions: {
    borderRadius: "50px",
    padding: "8px 20px",
    margin: "16px 0px",
    boxShadow: "1px 1px 8px 0px #3F51B5",
    // justifyContent: "center",
  },
  rootCard: {
    padding: theme.spacing(1, 0),
  },
  largeAvatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
  },
  cardHeader: {
    fontSize: window.innerHeight > 576 ? "large" : "medium",
    width: "max-content",
  },
  marginAction: {
    marginLeft: "0px !important",
  },
}));

export default useStyles;
