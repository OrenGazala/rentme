import React from "react";
import { Grid, Avatar, CardHeader, CardActions } from "@material-ui/core";
import { useFirestoreConnect, isLoaded, isEmpty } from "react-redux-firebase";
import { useSelector } from "react-redux";
import useStyles from "./styles";
import CircularLoading from "components/shared/circular-loading";
import { getUserById } from "queries/user-queries";

const ApartmentOwnerDetails = ({ uid }) => {
  const classes = useStyles();

  useFirestoreConnect(getUserById(uid));

  const user = useSelector((state) => state.firestore.data.users && state.firestore.data.users[uid]);

  if (!isLoaded(user)) return <CircularLoading />;
  if (isEmpty(user)) return <div></div>;

  return (
    <React.Fragment>
      <CardActions classes={{ root: classes.rootActions }}>
        <Grid item xs={8}>
          {/* <Link component={RouterLink} to={`/user-profile/${uid}`} underline="none" color="textPrimary"> */}
          <CardHeader
            classes={{
              root: classes.rootCard,
              title: classes.cardHeader,
              subheader: classes.cardHeader,
            }}
            avatar={
              <Avatar
                alt={user.firstName}
                src={user.profileImg ? user.profileImg : undefined}
                className={classes.largeAvatar}
              />
            }
            title={<span>{user.firstName + " " + user.lastName}</span>}
            subheader={user.email}
          />
        </Grid>
        {/* <Grid item xs={2} className={classes.marginAction}>
          <IconButton aria-label="like">
            <Favorite />
          </IconButton>
        </Grid>
        <Grid item xs={2} className={classes.marginAction}>
          <IconButton aria-label="dislike" className={classes.marginAction}>
            <CloseRounded />
          </IconButton>
        </Grid> */}
      </CardActions>
    </React.Fragment>
  );
};

export default ApartmentOwnerDetails;
