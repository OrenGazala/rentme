import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  numbersOnly: {
    "& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
      "-webkit-appearance": "none",
      margin: 0,
    },
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  image: {
    marginTop: 25,
    textAlign: "center",
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  pic: {
    marginLeft: 30,
    marginTop: 15,
    fontSize: 300,
  },
  card: {
    height: "100%",
    width: "30%",
    display: "flex",
    flexDirection: "column",
    position: "relative",
    marginLeft: 15,
    marginTop: 15,
  },
  cardMedia: {
    paddingTop: "56.25%",
  },
  cardContent: {
    flexGrow: 1,
  },
  del: {
    marginTop: -280,
    marginLeft: 200,
  },
  cardGrid: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
}));

export default useStyles;
