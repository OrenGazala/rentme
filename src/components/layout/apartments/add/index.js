import React, { useState, useRef, useCallback, useEffect } from "react";
import {
  Button,
  TextField,
  InputAdornment,
  Grid,
  MenuItem,
  Avatar,
  Typography,
  Slider,
  Chip,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Card,
  CardMedia,
  Container,
} from "@material-ui/core";

import {
  School,
  AttachMoney,
  Place,
  Hotel,
  HomeWork,
  LocalParking,
  DirectionsBus,
  Height,
  Deck,
  AcUnit,
  Weekend,
  ChildCare,
  MenuBook,
  LocalBar,
  SmokingRooms,
  Pets,
  Done,
  PhotoCamera,
  ExpandMore,
  Delete,
  People,
  Bathtub,
} from "@material-ui/icons";

import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  partnersMarks,
  bathroomsMarks,
  bedroomsMarks,
  propertyTypes,
} from "consts/shared-renter-aparment";
import { priceMarks } from "consts/shared-renter-aparment";

import { useHistory } from "react-router-dom";
import useStyles from "./styles";

import useCollection from "hooks/firebase/useCollection";
import useStorage from "hooks/firebase/useStorage";

import ApartmentModel from "models/apartment";
import { APARTMENTS_COLLECTION } from "consts/collections";

import { useSelector } from "react-redux";
import { isLoaded } from "react-redux-firebase";
import CircularLoading from "components/shared/circular-loading";

import useCityApi from "hooks/api/useCityApi";
import { debounce } from "lodash";

const AddApartmentForm = ({ apartmentId }) => {
  const classes = useStyles();
  const db = useCollection();
  const storage = useStorage();
  const history = useHistory();
  const cityApi = useCityApi();

  const { uid } = useSelector((state) => state.firebase.auth);
  const aparmentFromSelector = useSelector(
    (state) => state.firestore.data.apartments && state.firestore.data.apartments[apartmentId]
  );

  // Get the file browser input reference
  const inputFile = useRef(null);
  const [apartment, setApartment] = useState({
    ...ApartmentModel,
    partners: 1,
    images: [],
  });
  const [citiesApi, setCities] = useState([]);

  useEffect(() => {
    if (apartmentId) {
      if (!isLoaded(aparmentFromSelector)) return <CircularLoading></CircularLoading>;
      setApartment(aparmentFromSelector);
    }
  }, [apartmentId, aparmentFromSelector]);

  const indicationsChips = [
    { name: "hasParking", header: "Parking", icon: <LocalParking /> },
    { name: "hasPublicTransport", header: "Public Transport", icon: <DirectionsBus /> },
    { name: "hasElevator", header: "Elevator", icon: <Height /> },
    { name: "hasBalcony", header: "Balcony", icon: <Deck /> },
    { name: "hasAirConditioning", header: "Air Conditioning", icon: <AcUnit /> },
    { name: "hasFurnitures", header: "Furnitures", icon: <Weekend /> },
    { name: "hasSchool", header: "School", icon: <School /> },
    { name: "hasKinderGardens", header: "Kinder Gardens", icon: <ChildCare /> },
    { name: "hasSynagogue", header: "Synagogue", icon: <MenuBook /> },
    { name: "hasBars", header: "Bars", icon: <LocalBar /> },
    { name: "allowSmoke", header: "Allow Smoke", icon: <SmokingRooms /> },
    { name: "allowPet", header: "Allow Pet", icon: <Pets /> },
  ];

  const onUpload = () => {
    inputFile.current.click();
  };

  const chooseImage = async (e) => {
    if (e.target.files.length > 0) {
      const url = await storage.uploadFile(e.target.files[0], "apartment");
      if (url) {
        const images = [...apartment.images, { url: url }];
        setApartment({ ...apartment, images: images });
      }
    }
  };

  const handleSelect = (e) => {
    const { name, value } = e.target;
    setApartment({
      ...apartment,
      [name]: value,
    });
  };

  const handleCheckboxChange = (e) => {
    const name = e.currentTarget.title;
    setApartment({
      ...apartment,
      [name]: !apartment[name],
    });
  };

  const saveApartment = async (e) => {
    e.preventDefault();

    // Save the user id and images before saving
    var savedApartment = apartment;
    if (!savedApartment.uid) savedApartment.uid = uid;
    // savedApartment.images = [...images];
    var success;
    if (apartmentId) {
      success = await db.update({
        collection: APARTMENTS_COLLECTION,
        doc: apartmentId,
        data: savedApartment,
      });
    } else {
      success = await db.add({
        collection: APARTMENTS_COLLECTION,
        data: savedApartment,
      });
    }
    if (success) {
      history.push("/");
    }
  };

  const handlePriceChange = (event, newValue) => {
    setApartment({
      ...apartment,
      price: newValue,
    });
  };

  const handleBedroomsChange = (event, newValue) => {
    setApartment({
      ...apartment,
      bedrooms: newValue,
    });
  };

  const handleBathroomsChange = (event, newValue) => {
    setApartment({
      ...apartment,
      bathrooms: newValue,
    });
  };

  const handlePartnersChange = (event, newValue) => {
    setApartment({
      ...apartment,
      partners: newValue,
    });
  };

  const getCitiesLabels = () => {
    const labels = [];
    if (citiesApi) {
      citiesApi.map((option) => labels.push(option.region + " - " + option.name));
    }
    return labels;
  };

  const handleInput = (e, prefix) => {
    if (prefix.length > 1)
      cityApi.getCitiesByPrefix(prefix).then((res) => {
        setCities(res);
      });
  };
  const handler = useCallback(debounce(handleInput, 1000), []);
  const handleCityChange = (e, value) => {
    if (citiesApi)
      setApartment({
        ...apartment,
        city: { ...citiesApi.find((cityApi) => cityApi.region + " - " + cityApi.name === value) },
      });
  };

  const deleteImage = (e) => {
    var images = apartment.images;
    images = images.filter((img) => img.url !== e);
    setApartment({ ...apartment, images: images });
  };

  return (
    <form className={classes.form} onSubmit={saveApartment}>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6}>
          <Autocomplete
            id="combo-box-demo"
            options={getCitiesLabels()}
            onInputChange={handler}
            onChange={handleCityChange}
            getOptionLabel={(option) => option}
            value={apartment.city.id ? apartment.city.region + " - " + apartment.city.name : ""}
            renderInput={(params) => (
              <TextField
                {...params}
                required
                label="City"
                variant="outlined"
                helperText="What is your property city?"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            variant="outlined"
            required
            fullWidth
            label="Address"
            name="address"
            value={apartment.address}
            onChange={handleSelect}
            helperText="What is your property addrees?"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Place />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            variant="outlined"
            fullWidth
            required
            select
            label="Property Type"
            name="propertyType"
            value={apartment.propertyType}
            onChange={handleSelect}
            helperText="What property type you want?"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <HomeWork />
                </InputAdornment>
              ),
            }}
          >
            {Object.values(propertyTypes).map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item xs={12} sm={6}></Grid>
        <Grid container item xs={12} sm={6} spacing={0}>
          <Typography id="range-price" gutterBottom align="center">
            Apartment Price
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <AttachMoney />
            </Grid>
            <Grid item xs>
              <Slider
                valueLabelDisplay="auto"
                required
                value={apartment.price}
                onChange={handlePriceChange}
                min={0}
                max={9999}
                marks={priceMarks}
                step={50}
                aria-labelledby="range-price"
              />
            </Grid>
          </Grid>
          <Typography variant="caption" gutterBottom align="center" color="textSecondary">
            What is the your apartment price?
          </Typography>
        </Grid>
        <Grid container item xs={12} sm={6} spacing={0}>
          <Typography id="range-price" gutterBottom align="center">
            Partners
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <People />
            </Grid>
            <Grid item xs>
              <Slider
                valueLabelDisplay="auto"
                required
                value={apartment.partners}
                onChange={handlePartnersChange}
                min={1}
                max={10}
                marks={partnersMarks}
                aria-labelledby="partners"
              />
            </Grid>
          </Grid>
          <Typography variant="caption" gutterBottom align="center" color="textSecondary">
            How many partners in the property?
          </Typography>
        </Grid>
        <Grid container item xs={12} sm={6} spacing={0}>
          <Typography id="range-price" gutterBottom align="center">
            Bedrooms
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <Hotel />
            </Grid>
            <Grid item xs>
              <Slider
                valueLabelDisplay="auto"
                required
                value={apartment.bedrooms}
                onChange={handleBedroomsChange}
                min={0.5}
                max={10}
                marks={bedroomsMarks}
                step={0.5}
                aria-labelledby="bedrooms"
              />
            </Grid>
          </Grid>
          <Typography variant="caption" gutterBottom align="center" color="textSecondary">
            How many bedrooms in the property?
          </Typography>
        </Grid>
        <Grid container item xs={12} sm={6} spacing={0}>
          <Typography id="range-price" gutterBottom align="center">
            Bathrooms
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <Bathtub />
            </Grid>
            <Grid item xs>
              <Slider
                valueLabelDisplay="auto"
                required
                value={apartment.bathrooms}
                onChange={handleBathroomsChange}
                min={0}
                max={10}
                marks={bathroomsMarks}
                aria-labelledby="bathrooms"
              />
            </Grid>
          </Grid>
          <Typography variant="caption" gutterBottom align="center" color="textSecondary">
            How many bathrooms in the property?
          </Typography>
        </Grid>

        <Grid item xs={12} sm={12}></Grid>

        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMore />} aria-controls="panel1c-content">
            <div>Add More apartment Details</div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid item xs={12} sm={12}>
              <TextField
                variant="outlined"
                fullWidth
                label="Property description"
                name="description"
                value={apartment.description}
                onChange={handleSelect}
                multiline
              />
            </Grid>
          </ExpansionPanelDetails>
          <ExpansionPanelDetails>
            <Grid container spacing={1}>
              {indicationsChips.map((chip, index) => (
                <Grid item xs={12} sm={3} key={index}>
                  <Chip
                    variant={apartment[chip.name] ? "default" : "outlined"}
                    color={apartment[chip.name] ? "primary" : "default"}
                    className={classes.chip}
                    label={chip.header}
                    name={chip.name}
                    icon={chip.icon}
                    onClick={handleCheckboxChange}
                    deleteIcon={<Done />}
                    onDelete={apartment[chip.name] ? handleSelect : undefined}
                    title={chip.name}
                  />
                </Grid>
              ))}
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Grid>

      <Grid className={classes.image}>
        <input
          type="file"
          id="file"
          ref={inputFile}
          style={{ display: "none" }}
          onChange={chooseImage}
        />
        <Button onClick={onUpload}>
          <Avatar className={classes.avatar}>
            <PhotoCamera />
          </Avatar>
        </Button>
      </Grid>

      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          {apartment.images.length ? (
            apartment.images.map((image, index) => (
              <Card className={classes.card} key={index}>
                <CardMedia className={classes.cardMedia} image={image.url}>
                  <Button
                    className={classes.del}
                    variant="contained"
                    onClick={() => deleteImage(image.url)}
                  >
                    <Delete />
                  </Button>
                </CardMedia>
              </Card>
            ))
          ) : (
            <></>
          )}
        </Grid>
      </Container>
      <Grid item xs={12} sm={12} />

      <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
        Save
      </Button>
    </form>
  );
};

export default AddApartmentForm;
