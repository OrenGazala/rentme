import React from 'react';
import useAuth from 'hooks/firebase/useAuth';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from './styles';

const SignedInLinks = () => {
  const classes = useStyles();
  const authService = useAuth();

  const signOut = async () => {
    await authService.signOut();
  };

  return (
    <Button className={classes.margin} onClick={signOut} component={Link} to="/">
      Sign Out
    </Button>
  );
};

export default SignedInLinks;
