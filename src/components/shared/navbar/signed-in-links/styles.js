import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(0, 1),
    textTransform: "none",
  },
}));

export default useStyles;
