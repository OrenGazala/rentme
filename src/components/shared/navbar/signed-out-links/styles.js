import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  lowerCase: {
    textTransform: "none",
  },
}));

export default useStyles;
