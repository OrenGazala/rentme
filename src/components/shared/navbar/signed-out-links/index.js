import React from "react";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import useStyles from "./styles";

const SignedOutLinks = () => {
  const classes = useStyles();

  return (
    <>
      <Button className={classes.lowerCase} component={Link} to="/signin">
        Sign In
      </Button>
      <Button className={classes.lowerCase} component={Link} to="/signup">
        Sign Up
      </Button>
    </>
  );
};

export default SignedOutLinks;
