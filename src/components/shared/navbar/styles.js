import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    textTransform: "none",
    padding: theme.spacing(0, 1),
  },
  margin: {
    margin: theme.spacing(0, 1),
    textTransform: "none",
  },
  sign: {
    right: "0px",
    position: "absolute",
  },
}));

export default useStyles;
