import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import SignedOutLinks from "./signed-out-links";
import SignedInLinks from "./signed-in-links";
import { AppBar, Toolbar, Button, Hidden } from "@material-ui/core";
import useStyles from "./styles";

const Navbar = () => {
  const classes = useStyles();
  const { uid } = useSelector((state) => state.firebase.auth);

  const Links = [
    { name: "My Apartments", path: `/my-apartments` },
    { name: "Favorite", path: `/favorite-apartments` },
    { name: "Profile", path: `/user-profile/${uid}` },
  ];

  const signedLinks = uid ? <SignedInLinks /> : <SignedOutLinks />;

  return (
    <AppBar position="static" className={classes.appbar}>
      <Toolbar>
        <Button size="large" className={classes.title} component={Link} to="/">
          RentMe
        </Button>
        <Hidden smDown>
          {Links.map((link) => (
            <Button
              key={link.name}
              variant="outlined"
              className={classes.margin}
              component={Link}
              to={uid ? link.path : "/signin"}
            >
              {link.name}
            </Button>
          ))}
        </Hidden>
        <div className={classes.sign}>{signedLinks}</div>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
