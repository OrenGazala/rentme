import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(3),
    width: "100%",
    position: "relative",
    bottom: "0px",
  },
}));

export default useStyles;
