import React from "react";
import { Backdrop, CircularProgress, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

const BackdropCircularLoading = () => {
  const classes = useStyles();
  return (
    <Backdrop className={classes.backdrop} open={true}>
      <CircularProgress></CircularProgress>
    </Backdrop>
  );
};

export default BackdropCircularLoading;
