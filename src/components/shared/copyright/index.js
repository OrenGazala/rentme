import React from "react";
import { Typography } from "@material-ui/core";

const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © 2020 RentMe, Inc. All rights reserved"}
    </Typography>
  );
};

export default Copyright;
