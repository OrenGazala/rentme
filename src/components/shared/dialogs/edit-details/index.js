import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  InputAdornment,
  Divider,
} from "@material-ui/core";
import { useSelector } from "react-redux";
import { AccountCircle, Phone } from "@material-ui/icons";
import useCollection from "hooks/firebase/useCollection";
import { USERS_COLLECTION } from "consts/collections";

const EditDetailsDialog = ({ open, onCancel }) => {
  const { uid } = useSelector((state) => state.firebase.auth);
  const profile = useSelector((state) => state.firestore.data.users && state.firestore.data.users[uid]);

  const db = useCollection();
  const [user, setUser] = useState(profile);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({
      ...user,
      [name]: value,
    });
  };

  const editUser = async (e) => {
    e.preventDefault();
    const success = await db.update({
      collection: USERS_COLLECTION,
      doc: uid,
      data: user,
    });
    if (success) onCancel();
  };

  return (
    <React.Fragment>
      <Dialog open={open} onClose={onCancel}>
        <DialogTitle>Edit Details</DialogTitle>
        <Divider />
        <br />
        <DialogContent>
          <Grid item xs={12} style={{ marginBottom: "16px" }}>
            <TextField
              variant="outlined"
              required
              fullWidth
              label="First Name"
              name="firstName"
              autoFocus
              value={user.firstName}
              onChange={handleChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          <Grid item xs={12} style={{ marginBottom: "16px" }}>
            <TextField
              variant="outlined"
              required
              fullWidth
              label="Last Name"
              name="lastName"
              value={user.lastName}
              onChange={handleChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          <Grid item xs={12} style={{ marginBottom: "16px" }}>
            <TextField
              variant="outlined"
              required
              fullWidth
              label="Phone Number"
              name="phoneNumber"
              value={user.phoneNumber}
              onChange={handleChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Phone />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="primary" autoFocus>
            Close
          </Button>
          <Button onClick={editUser} color="primary" autoFocus>
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default EditDetailsDialog;
