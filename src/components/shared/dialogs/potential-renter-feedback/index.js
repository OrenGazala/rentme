import React from "react";
import { USERS_COLLECTION } from "consts/collections";
import { Dialog, DialogTitle, DialogContent } from "@material-ui/core";
import AddFeedback from "components/layout/feedbacks/add";

const PotentialRenterFeedback = ({ uid, apartmentID, open, handleClose }) => {
  return (
    <Dialog fullWidth={true} maxWidth={"md"} open={open} aria-labelledby="max-width-dialog-title">
      <DialogTitle id="max-width-dialog-title">Please rate him</DialogTitle>
      <DialogContent>
        <AddFeedback
          collection={USERS_COLLECTION}
          uid={uid}
          apartmentID={apartmentID}
          onAdd={handleClose}
        />
      </DialogContent>
    </Dialog>
  );
};

export default PotentialRenterFeedback;
