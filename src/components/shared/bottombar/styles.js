import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    whiteSpace: "pre",
    display: "flex",
    position: "fixed",
    zIndex: 660,
    bottom: 0,
    width: "100%",
    marginTop: theme.spacing(2),
  },
}));

export default useStyles;
