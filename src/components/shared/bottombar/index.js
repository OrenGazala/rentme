import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link as RouterLink } from "react-router-dom";

import { BottomNavigation, BottomNavigationAction, Hidden } from "@material-ui/core";
// import { RiParentLine } from "react-icons/ri";
import useStyles from "./styles";
import { AccountCircle, Favorite, Home, HomeWork } from "@material-ui/icons";
import { saveTabIndexAction } from "store/actions/ui-actions";

/* Display this Bottom bar ONLY when the user is sign on and we are watching on mobile  */
const Bottombar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { uid } = useSelector((state) => state.firebase.auth);
  const { tabIndex } = useSelector((state) => state.ui);

  const bottomOptions = [
    { tabName: "Home", path: "/", icon: <Home /> },
    { tabName: "Favorites", path: "/favorite-apartments", icon: <Favorite /> },
    { tabName: "My Apartments", path: "/my-apartments", icon: <HomeWork /> },
    { tabName: "Profile", path: `/user-profile/${uid}`, icon: <AccountCircle /> },
  ];

  return (
    <Hidden smUp>
      <BottomNavigation
        value={tabIndex}
        onChange={(event, newIndex) => {
          dispatch(saveTabIndexAction(newIndex));
        }}
        showLabels
        className={classes.root}
      >
        {bottomOptions.map((data, index) => (
          <BottomNavigationAction
            key={index}
            component={RouterLink}
            to={uid || data.path === "/" ? data.path : "/signin"}
            label={data.tabName}
            icon={data.icon}
          />
        ))}
      </BottomNavigation>
    </Hidden>
  );
};

export default Bottombar;
