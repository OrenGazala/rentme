import React from "react";
import { CircularProgress, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  center: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: "16px 0px",
  },
}));

const CircularLoading = () => {
  const classes = useStyles();
  return (
    <div className={classes.center}>
      <CircularProgress></CircularProgress>
    </div>
  );
};

export default CircularLoading;
