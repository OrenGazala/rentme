import { createMuiTheme } from "@material-ui/core";

const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
    // background: "#fafafa",
    primary: {
      // light: "#a6d4fa",
      main: "#11cbd7",
      // dark: "#648dae",
    },
    secondary: {
      // light: "#f6a5c0",
      main: "#fa4659",
      // dark: "#aa647b",
    },
  },
});

export default darkTheme;
