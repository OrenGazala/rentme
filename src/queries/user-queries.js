import { USERS_COLLECTION } from "consts/collections";

export const getUserById = (doc) => {
  return {
    collection: USERS_COLLECTION,
    doc,
  };
};
