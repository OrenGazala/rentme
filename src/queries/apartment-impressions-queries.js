import { APARTMENT_IMPRESSIONS_COLLECTION } from "consts/collections";

export const getImpressionByApartment = (apartmentID) => {
  return {
    collection: APARTMENT_IMPRESSIONS_COLLECTION,
    where: [
      ["apartmentID", "==", apartmentID],
      ["liked", "==", true],
      ["endDate", "==", null],
    ],
    storeAs: `apartment_impressions_${apartmentID}`,
  };
};

export const getImpressionByUID = (uid) => {
  return {
    collection: APARTMENT_IMPRESSIONS_COLLECTION,
    where: [
      ["uid", "==", uid],
      ["liked", "==", true],
      ["endDate", "==", null],
    ],
  };
};
