import { APARTMENTS_COLLECTION } from "consts/collections";

export const getApartmentsByUID = (uid) => {
  return {
    collection: APARTMENTS_COLLECTION,
    where: [
      ["uid", "==", uid],
      ["endDate", "==", null],
    ],
  };
};
