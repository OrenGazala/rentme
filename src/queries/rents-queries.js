import { RENTS_COLLECTION } from "consts/collections";

export const getRentsByUIDAndApartmentID = (uid, apartmentID) => {
  return {
    collection: RENTS_COLLECTION,
    where: [
      ["uid", "==", uid],
      ["apartmentID", "==", apartmentID],
      ["endDate", "==", null],
    ],
  };
};

export const getRentsByUID = (uid) => {
  return {
    collection: RENTS_COLLECTION,
    where: [
      ["uid", "==", uid],
      ["endDate", "==", null],
    ],
  };
};
