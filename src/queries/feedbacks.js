import { FEEDBACKS_COLLECTION } from "consts/collections";

export const getFeedbacks = (collection, doc) => {
  return {
    collection,
    doc,
    subcollections: [{ collection: FEEDBACKS_COLLECTION }],
    storeAs: FEEDBACKS_COLLECTION,
    where: [["endDate", "==", null]],
  };
};
