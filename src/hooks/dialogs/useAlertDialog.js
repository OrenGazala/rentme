import React from "react";
import AlertDialog from "components/shared/dialogs/alert";
import { useModal } from "react-modal-hook";

const useAlertDialog = alert => {
  const [showAlertDialog, hideAlertDialog] = useModal(() => {
    return (
      <AlertDialog open={true} title={alert.title} message={alert.message} onCancel={hideAlertDialog} />
    );
  }, [alert]);

  return showAlertDialog;
};

export default useAlertDialog;
