import axios from "axios";
import usePromiseHandler from "hooks/handlers/usePromiseHandler";

const useCityApi = () => {
  const handle = usePromiseHandler();
  const countryIds = "US";

  const getRegionsByPrefix = async (namePrefix) => {
    const response = await handle(
      axios({
        method: "GET",
        url: `https://wft-geo-db.p.rapidapi.com/v1/geo/countries/${countryIds}/regions`,
        headers: {
          "content-type": "application/octet-stream",
          "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
          "x-rapidapi-key": "0b6aadfe4dmshef694902c1151b2p1856c0jsnb7cc36ac4c31",
          useQueryString: true,
        },
        params: {
          limit: "5",
          namePrefix,
        },
      })
    );

    if (response) {
      const regions = response.data.data.map((country) => {
        return {
          id: country.code,
          name: country.name,
        };
      });

      return regions;
    }
  };

  const getCitiesByPrefix = async (namePrefix) => {
    const response = await handle(
      axios({
        method: "GET",
        url: "https://wft-geo-db.p.rapidapi.com/v1/geo/cities",
        headers: {
          "content-type": "application/octet-stream",
          "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
          "x-rapidapi-key": "0b6aadfe4dmshef694902c1151b2p1856c0jsnb7cc36ac4c31",
          useQueryString: true,
        },
        params: {
          countryIds,
          namePrefix,
          types: "city",
          limit: "5",
        },
      })
    );
    if (response) {
      const cities = response.data.data.map((city) => {
        return {
          id: city.id,
          name: city.name,
          region: city.region,
          latitude: city.latitude,
          longitude: city.longitude,
        };
      });

      return cities;
    }
  };

  return {
    getRegionsByPrefix,
    getCitiesByPrefix,
  };
};

export default useCityApi;
