import useAlertDialog from "hooks/dialogs/useAlertDialog";
import { useState } from "react";

const usePromiseHandler = () => {
  const [alertData, setAlertData] = useState({ title: "", message: "" });
  const showAlertDialog = useAlertDialog(alertData);

  const handle = async (promise) => {
    try {
      return await promise;
    } catch (err) {
      setAlertData({
        title: err.name,
        message: err.message,
      });
      console.error(err.name, err.message, err.details);
      showAlertDialog();
    }
  };

  return handle;
};

export default usePromiseHandler;
