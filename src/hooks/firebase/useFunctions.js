import { useFirebase } from "react-redux-firebase";
import usePromiseHandler from "hooks/handlers/usePromiseHandler";

const useFunctions = () => {
  const firebase = useFirebase();
  const handle = usePromiseHandler();

  const call = async (name, params = {}) => {
    const func = firebase.functions().httpsCallable(name);
    const res = await handle(func({ ...params }));
    if (res) return res.data;
  };

  return {
    call,
  };
};
export default useFunctions;
