import { useFirebase } from "react-redux-firebase";
import usePromiseHandler from "hooks/handlers/usePromiseHandler";

const useStorage = () => {
  const firebase = useFirebase();
  const storage = firebase.storage();
  const handle = usePromiseHandler();

  const uploadFile = async (file, from) => {
    const path = from + "/" + new Date().getTime() + file.name;
    await handle(storage.ref(`images/${path}`).put(file));
    return await handle(storage.ref("images").child(path).getDownloadURL());
  };

  return {
    uploadFile,
  };
};

export default useStorage;
