import { useFirestore } from "react-redux-firebase";
import usePromiseHandler from "hooks/handlers/usePromiseHandler";

const useCollection = () => {
  const firestore = useFirestore();
  const handle = usePromiseHandler();

  const listen = async (query) => {
    await firestore.setListener(query);
    return () => firestore.unsetListener(query);
  };

  const add = async ({ collection, data }) => {
    return await handle(
      firestore.collection(collection).add({
        ...data,
        startDate: new Date(),
        endDate: null,
      })
    );
  };

  const addToSubCollection = async ({ collection, doc, subCollection, data }) => {
    return await handle(
      firestore.collection(`${collection}/${doc}/${subCollection}`).add({
        ...data,
        startDate: new Date(),
        endDate: null,
      })
    );
  };

  const update = async ({ collection, doc, data }) => {
    return await handle(
      firestore
        .collection(collection)
        .doc(doc)
        .update({ ...data })
        .then(() => true)
    );
  };

  const updateToSubCollection = async ({ collection, doc, subCollection, subCollectionDoc, data }) => {
    return await handle(
      firestore
        .collection(`${collection}/${doc}/${subCollection}`)
        .doc(subCollectionDoc)
        .update({ ...data })
        .then(() => true)
    );
  };

  const remove = async ({ collection, doc }) => {
    return await handle(
      firestore
        .collection(collection)
        .doc(doc)
        .update({
          endDate: new Date(),
        })
        .then(() => true)
    );
  };

  const removeToSubCollection = async ({ collection, doc, subCollection, subCollectionDoc }) => {
    return await handle(
      firestore
        .collection(`${collection}/${doc}/${subCollection}`)
        .doc(subCollectionDoc)
        .update({
          endDate: new Date(),
        })
        .then(() => true)
    );
  };

  return {
    listen,
    add,
    addToSubCollection,
    update,
    updateToSubCollection,
    remove,
    removeToSubCollection,
  };
};

export default useCollection;
