import { useFirebase, useFirestore } from 'react-redux-firebase';
import { USERS_COLLECTION } from 'consts/collections';
import usePromiseHandler from 'hooks/handlers/usePromiseHandler';
import { useDispatch } from 'react-redux';
import { getApartmentsAction } from 'store/actions/apartments-actions';

/**
 * Hooks for user Authentication
 */
const useAuth = () => {
  const firebase = useFirebase();
  const firestore = useFirestore();
  const dispatch = useDispatch();
  const handle = usePromiseHandler();

  const signInWithEmailAndPassword = async (email, password) => {
    clearApartmentsReducer();
    return await handle(firebase.auth().signInWithEmailAndPassword(email, password));
  };

  const createUserWithEmailAndPassword = (user) => {
    clearApartmentsReducer();
    const { password, ...userData } = user;
    return handle(
      firebase
        .auth()
        .createUserWithEmailAndPassword(user.email, password)
        .then((auth) => {
          return firestore
            .collection(USERS_COLLECTION)
            .doc(auth.user.uid)
            .set({ ...userData })
            .then(() => true);
        })
    );
  };

  const signOut = () => {
    clearApartmentsReducer();
    return handle(firebase.auth().signOut());
  };

  const clearApartmentsReducer = () => {
    dispatch(getApartmentsAction({}, false, true));
  };

  return {
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    signOut,
  };
};

export default useAuth;
