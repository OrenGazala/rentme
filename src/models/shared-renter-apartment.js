const SharedRenterApartment = {
  bathrooms: 0,
  bedrooms: 0,
  propertyType: "",
  city: {
    id: 0,
    name: "",
    region: "",
    latitude: 0,
    longitude: 0,
  },
  hasParking: false,
  hasPublicTransport: false,
  hasElevator: false,
  hasBalcony: false,
  hasAirConditioning: false,
  hasFurnitures: false,
  hasSchool: false,
  hasKinderGardens: false,
  hasSynagogue: false,
  hasBars: false,
  allowSmoke: false,
  allowPet: false,
  partners: 0,
};

export default SharedRenterApartment;
