import SharedRenterApartment from "./shared-renter-apartment";

const ApartmentModel = {
  price: 0,
  address: "",
  uid: "",
  images: [],
  description: "",
  active: true,
  ...SharedRenterApartment,
  feedbackDetails: {
    avg: 0,
    count: 0,
  },
};

export default ApartmentModel;
