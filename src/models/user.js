import RenterModel from "./renter";

const birthDate = new Date();
birthDate.setFullYear(birthDate.getFullYear() - 18);

const UserModel = {
  IDNumber: "",
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  phoneNumber: "",
  birthDate,
  profileImg: "",
  renterProps: {
    ...RenterModel,
  },
  feedbackDetails: {
    avg: 0,
    count: 0,
  },
};

export default UserModel;
