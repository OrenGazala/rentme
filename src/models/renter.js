import SharedRenterApartment from "./shared-renter-apartment";

const RenterModel = {
  salary: 0,
  priceLimit: 0,
  job: "",
  familyStatus: "",
  education: 0,
  ...SharedRenterApartment,
};

export default RenterModel;
