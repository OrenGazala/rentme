import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";
import "firebase/auth";
import firebaseConfig from "./config/fb-conf";
import ReactDOM from "react-dom";
import App from "./App";
import { store, persistor } from "./store/store";
import * as serviceWorker from "./serviceWorker";
import { Provider, useSelector } from "react-redux";
import { ReactReduxFirebaseProvider, isLoaded } from "react-redux-firebase";
import { createFirestoreInstance } from "redux-firestore";
import "./index.css";
import { PersistGate } from "redux-persist/integration/react";

firebase.initializeApp(firebaseConfig);
firebase.firestore();
firebase.functions();

const rrfConfig = {
  userProfile: "users",
  useFirestoreForProfile: true,
  enableClaims: true,
};

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

const AuthIsLoaded = ({ children }) => {
  const { auth } = useSelector((state) => state.firebase);
  if (!isLoaded(auth)) return <div></div>;

  return children;
};

ReactDOM.render(
  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rrfProps}>
      <AuthIsLoaded>
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </AuthIsLoaded>
    </ReactReduxFirebaseProvider>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
