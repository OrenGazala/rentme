const triggers = require("./src/triggers");
const httpFunctions = require("./src/http-functions");

// Export DB functions
exports.functions = httpFunctions;

// Export DB triggers
exports.triggers = triggers;
