const fs = require("fs");
const path = require("path");

// ES2019
exports.fromEntries = (iterable) => {
  return [...iterable].reduce((obj, [key, val]) => {
    obj[key] = val;
    return obj;
  }, {});
};
