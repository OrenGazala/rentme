const { calcMatchLevel } = require("./match-level");
const { SALARIES, EDUCATIONS, FAMILY_STATUSES } = require("../consts/renter-props");
const { getCreditScore } = require("../queries/credit-score");

exports.calcAgreementLevel = async (user, apartment) => {
  const score = await getScore(user, apartment);
  const weight = getWeight();

  let gradeWithoutFeedback = 0;
  Object.entries(score).forEach(([key, value]) => (gradeWithoutFeedback += value * weight[key]));

  const feedbackWeight = calcFeedbackWeight(user.feedbackDetails.count);

  let grade = feedbackWeight * user.feedbackDetails.avg + (1 - feedbackWeight) * gradeWithoutFeedback;

  grade /= 10;

  grade = grade.toFixed(1);

  return grade;
};

const getWeight = () => {
  return {
    creditScore: 0.6,
    matchLevel: 0.25,
    salary: 0.07,
    familyStatus: 0.04,
    education: 0.04,
  };
};

const getScore = async (user, apartment) => {
  const creditScore = await getCreditScore(user.IDNumber);
  const matchLevel = calcMatchLevel(user, apartment);
  return {
    creditScore: calcCreditScore(creditScore),
    matchLevel,
    salary: calcSalary(user.renterProps.salary),
    familyStatus: calcFamilyStatus(user.renterProps.familyStatus),
    education: calcEducation(user.renterProps.education),
  };
};

const calcCreditScore = (creditScore) => {
  if (creditScore < 300 || creditScore > 850) return 0;
  if (creditScore <= 600) return (creditScore - 300) / 15;
  if (creditScore <= 800) return (creditScore - 600) / 2.7 + 20;
  return (creditScore - 800) / 10 + 95;
};

const calcSalary = (salary) => {
  switch (salary) {
    case SALARIES.VERY_LOW:
      return 20;
    case SALARIES.LOW:
      return 40;
    case SALARIES.MEDIUM:
      return 60;
    case SALARIES.HIGH:
      return 80;
    case SALARIES.VERY_HIGH:
      return 100;
    default:
      return 0;
  }
};

const calcFamilyStatus = (familyStatus) => {
  switch (familyStatus) {
    case FAMILY_STATUSES.SINGLE:
      return 50;
    case FAMILY_STATUSES.DIVORCED:
      return 60;
    case FAMILY_STATUSES.MARRIED:
      return 80;
    case FAMILY_STATUSES.WIDOW:
      return 90;
    case FAMILY_STATUSES.RELATIONSHIP:
      return 100;
    default:
      return 0;
  }
};

const calcEducation = (education) => {
  switch (education) {
    case EDUCATIONS.ELEMENTRY_SCHOOL:
      return 20;
    case EDUCATIONS.HIGH_SCHOOL:
      return 50;
    case EDUCATIONS.COLLEAGE:
      return 70;
    case EDUCATIONS.BACHELOR_DEGREE:
      return 80;
    case EDUCATIONS.ASSOCIATE_DEGREE:
      return 90;
    case EDUCATIONS.DOCTORATE_DEGREE:
      return 100;
    default:
      return 0;
  }
};

const calcFeedbackWeight = (countFeedback) => {
  if (countFeedback >= 100) return 0.35;
  return (countFeedback / 100) * 0.35;
};
