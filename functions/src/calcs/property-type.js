const { PROPERTY_TYPES } = require("../consts/renter-props");

const sortedPropertyType = [
  PROPERTY_TYPES.APARTMENT,
  PROPERTY_TYPES.PENTHOUSE,
  PROPERTY_TYPES.DUPLEX,
  PROPERTY_TYPES.GARDEN_APARTMENT,
  PROPERTY_TYPES.VILA,
];

exports.getAllNearnessPropertyType = (propertyType) => {
  const index = sortedPropertyType.findIndex((value) => value === propertyType);
  let start = 0;
  if (index !== 0) start = index - 1;
  let end = 0;
  if (end !== sortedPropertyType.length) end = index + 1;

  return sortedPropertyType.slice(start, end + 1);
};

exports.sortedPropertyType = sortedPropertyType;
