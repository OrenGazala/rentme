const { getAllNearnessPropertyType } = require("./property-type");
const { getDistance } = require("../api/city");
const { exportFile } = require("../utils");

const functions = require("firebase-functions");

exports.calcMatchLevel = (user, apartment) => {
  try {
    const score = getScore(user.renterProps, apartment);
    const weight = getWeight();

    let gradeWithoutFeedback = 0;
    Object.entries(score).forEach(([key, value]) => (gradeWithoutFeedback += value * weight[key]));

    const feedbackWeight = calcFeedbackWeight(apartment.feedbackDetails.count);

    let grade =
      feedbackWeight * apartment.feedbackDetails.avg + (1 - feedbackWeight) * gradeWithoutFeedback;

    grade /= 10;

    grade = grade.toFixed(1);

    writeToLog({ user, apartment, score, weight, gradeWithoutFeedback, feedbackWeight, grade });

    return grade;
  } catch (err) {
    throw new functions.https.HttpsError("aborted", err.message, "calcMatchLevel");
  }
};

const getScore = (user, apartment) => {
  return {
    price: calcPrice(user.priceLimit, apartment.price),
    bedrooms: calcBedroom(user.bedrooms, apartment.bedrooms),
    propertyType: calcPropertyType(user.propertyType, apartment.propertyType),

    city: calcCity(user.city, apartment.city),

    bathrooms: calcBathrooms(user.bathrooms, apartment.bathrooms),
    partners: calcPartners(user.partners, apartment.partners),

    hasParking: calcIndication(user.hasParking, apartment.hasParking),
    hasAirConditioning: calcIndication(user.hasAirConditionin, apartment.hasAirConditionin),
    hasBalcony: calcIndication(user.hasBalcony, apartment.hasBalcony),
    hasElevator: calcIndication(user.hasElevator, apartment.hasElevator),
    hasKinderGardens: calcIndication(user.hasKinderGardens, apartment.hasKinderGardens),

    hasPublicTransport: calcIndication(user.hasPublicTranspor, apartment.hasPublicTranspor),
    hasFurnitures: calcIndication(user.hasFurnitures, apartment.hasFurnitures),
    hasSchool: calcIndication(user.hasSchool, apartment.hasSchool),
    allowSmoke: calcIndication(user.allowSmoke, apartment.allowSmoke),

    hasSynagogue: calcIndication(user.hasSynagogue, apartment.hasSynagogue),
    hasBars: calcIndication(user.hasBars, apartment.hasBars),
    allowPet: calcIndication(user.allowPet, apartment.allowPet),
  };
};

const getWeight = () => {
  return {
    price: 0.25,
    bedrooms: 0.15,
    propertyType: 0.14,

    city: 0.1,

    bathrooms: 0.05,
    partners: 0.05,

    hasParking: 0.03,
    hasAirConditioning: 0.03,
    hasBalcony: 0.03,
    hasElevator: 0.03,
    hasKinderGardens: 0.03,

    hasPublicTransport: 0.02,
    hasFurnitures: 0.02,
    hasSchool: 0.02,
    allowSmoke: 0.02,

    hasSynagogue: 0.01,
    hasBars: 0.01,
    allowPet: 0.01,
  };
};

const calcBedroom = (userBedrooms, apartmentBedrooms) => {
  const sub = userBedrooms - apartmentBedrooms;
  if (sub === 0) return 100;
  if (sub === -0.5) return 90;
  if (sub === 0.5) return 80;
  if (sub === -1) return 70;
  if (sub === 1) return 60;
  if (sub === -1.5) return 40;
  if (sub === 1.5) return 20;
  return 0;
};

const calcBathrooms = (userBathrooms, apartmentBathrooms) => {
  const sub = Math.abs(userBathrooms - apartmentBathrooms);
  if (sub === 0) return 100;
  if (sub === 1) return 70;
  if (sub === 2) return 40;
  return 0;
};

const calcCity = (userCity, apartmentCity) => {
  const distance = getDistance(
    userCity.latitude,
    userCity.longitude,
    apartmentCity.latitude,
    apartmentCity.longitude
  );

  if (distance < 1) return 100;
  if (distance > 16) return 0;
  return 100 - (distance / 16) * 100;
};

const calcPropertyType = (userPropertyType, apartmentPropertyType) => {
  if (userPropertyType === apartmentPropertyType) return 100;
  if (apartmentPropertyType in getAllNearnessPropertyType(userPropertyType)) return 50;
  return 0;
};

const calcPartners = (userPartners, apartmentPartners) => {
  const sub = Math.abs(userPartners - apartmentPartners);
  if (userPartners === 1) {
    if (apartmentPartners === 1) return 100;
    if (apartmentPartners === 2) return 50;
  } else {
    if (sub === 0) return 100;
    if (sub === 1) return 80;
    if (sub === 2) return 50;
  }
  return 0;
};

const calcPrice = (userPrice, apartmentPrice) => {
  const percent = (apartmentPrice / userPrice - 1) * 100;
  if (userPrice >= apartmentPrice) return 100;
  if (percent >= 25) return 0;
  return 100 - percent * 4;
};

const calcIndication = (userInd, apartmentInd) => {
  if (userInd && !apartmentInd) return 0;
  return 100;
};

const calcFeedbackWeight = (countFeedback) => {
  if (countFeedback >= 100) return 0.25;
  return (countFeedback / 100) * 0.25;
};

const writeToLog = (data) => {
  if (data.user.email === "oren@gmail.com" || data.user.email === "oz@gmail.com") console.log(data);
};
