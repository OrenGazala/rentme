exports.calcFeedbackDetails = (oldFeedbackDetails, snap) => {
  const oldFeedback = snap.before.exists ? snap.before.data() : null;
  const newFeedback = snap.after.exists ? snap.after.data() : null;

  const feedbackDetails = calcNewFeedbackDetails(oldFeedbackDetails, oldFeedback, newFeedback);

  return feedbackDetails;
};

const calcNewFeedbackDetails = (oldFeedbackDetails, oldFeedback, newFeedback) => {
  let newFeedbackDetails = {};

  // update has both
  if (oldFeedback && newFeedback) {
    // update
    newFeedbackDetails = calcUpdate(oldFeedbackDetails, oldFeedback.rating, newFeedback.rating);
  } else if (oldFeedback) {
    // delete
    newFeedbackDetails = calcDelete(oldFeedbackDetails, oldFeedback.rating);
  } else if (newFeedback) {
    // add new feedback
    newFeedbackDetails = calcCreate(oldFeedbackDetails, newFeedback.rating);
  }

  return newFeedbackDetails;
};

const calcCreate = (oldFeedbackDetails, newRating) => {
  return {
    avg:
      (oldFeedbackDetails.avg * oldFeedbackDetails.count + newRating) / (oldFeedbackDetails.count + 1),
    count: oldFeedbackDetails.count + 1,
  };
};

const calcUpdate = (oldFeedbackDetails, oldRating, newRating) => {
  return {
    avg:
      (oldFeedbackDetails.avg * oldFeedbackDetails.count - oldRating + newRating) /
      oldFeedbackDetails.count,
    count: oldFeedbackDetails.count,
  };
};

const calcDelete = (oldFeedbackDetails, oldRating) => {
  // if last feedback
  if (oldFeedbackDetails.count <= 1) {
    return { avg: 0, count: 0 };
  } else {
    return {
      avg:
        (oldFeedbackDetails.avg * oldFeedbackDetails.count - oldRating) / (oldFeedbackDetails.count - 1),
      count: oldFeedbackDetails.count - 1,
    };
  }
};
