const { APARTMENTS_COLLECTION } = require("../consts/collections");
const { getAllNearnessPropertyType } = require("../calcs/property-type");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");
const db = admin.firestore();

exports.getApartmentsByRenterProps = async (
  { uid, region, propertyType, price, bedrooms },
  onePropertyType = true
) => {
  try {
    let ref = db
      .collection(APARTMENTS_COLLECTION)
      .where("active", "==", true)
      .where("endDate", "==", null)
      .where("bedrooms", ">=", bedrooms[0])
      .where("bedrooms", "<=", bedrooms[1])
      .where("city.region", "==", region);

    if (onePropertyType) {
      ref = ref.where("propertyType", "==", propertyType);
    } else {
      const propertyTypes = getAllNearnessPropertyType(propertyType);
      ref = ref.where("propertyType", "in", propertyTypes);
    }

    const docs = (await ref.get()).docs;

    const data = {};
    docs.forEach((doc) => {
      const key = doc.id;
      const apartment = doc.data();
      if (apartment.uid !== uid && apartment.price <= price) data[key] = apartment;
    });

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getApartments");
  }
};

exports.getRandomApartments = async (uid, numberOfApartments, apartmentsNotInclude = {}) => {
  try {
    const orderKey = Math.random() > 0.5 ? "asc" : "desc";

    const docs = (
      await db
        .collection(APARTMENTS_COLLECTION)
        .where("active", "==", true)
        .where("endDate", "==", null)
        .orderBy(admin.firestore.FieldPath.documentId(), orderKey)
        .limit(100)
        .get()
    ).docs;

    const data = {};
    docs
      .sort(() => Math.random() > 0.5)
      .forEach((doc) => {
        const apartment = doc.data();
        const apartmentID = doc.id;
        if (
          (!uid || (uid && uid !== apartment.uid)) &&
          numberOfApartments &&
          !(apartmentID in apartmentsNotInclude)
        ) {
          data[apartmentID] = apartment;
          numberOfApartments--;
        }
      });

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getRandomApartments");
  }
};

exports.getApartmentByID = async (id) => {
  try {
    const apartment = (await db.doc(`${APARTMENTS_COLLECTION}/${id}`).get()).data();
    return apartment;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getApartmentByID");
  }
};

exports.getApartmentsByIds = async (ids) => {
  try {
    if (!ids) return {};

    const promisesToAwait = [];
    while (ids.length) {
      const ids10 = ids.splice(0, 10);
      const promise = db
        .collection(APARTMENTS_COLLECTION)
        .where(admin.firestore.FieldPath.documentId(), "in", ids10)
        .get();
      promisesToAwait.push(promise);
    }

    const data = {};
    const responses = await Promise.all(promisesToAwait);
    responses.forEach((response) => {
      response.docs.forEach((doc) => (data[doc.id] = doc.data()));
    });

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getApartmentsByIds");
  }
};
