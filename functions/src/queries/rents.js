const { RENTS_COLLECTION } = require("../consts/collections");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");
const db = admin.firestore();

exports.getRentsByApartmentID = async (apartmentID) => {
  try {
    const docs = (
      await db
        .collection(RENTS_COLLECTION)
        .where("apartmentID", "==", apartmentID)
        .where("endDate", "==", null)
        .get()
    ).docs;

    const data = {};
    docs.forEach((doc) => (data[doc.id] = doc.data()));

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getRentsByApartmentID");
  }
};

exports.getRentsByUID = async (uid) => {
  try {
    const docs = (
      await db.collection(RENTS_COLLECTION).where("uid", "==", uid).where("endDate", "==", null).get()
    ).docs;

    const data = {};
    docs.forEach((doc) => (data[doc.id] = doc.data()));

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getRentsByUID");
  }
};
