const { APARTMENT_IMPRESSIONS_COLLECTION } = require("../consts/collections");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");
const db = admin.firestore();

exports.getAllLikeApartmentsByUID = async (uid) => {
  try {
    const docs = (
      await db
        .collection(APARTMENT_IMPRESSIONS_COLLECTION)
        .where("uid", "==", uid)
        .where("endDate", "==", null)
        .get()
    ).docs;
    const data = {};
    docs.forEach((doc) => (data[doc.id] = doc.data()));

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getAllLikeApartmentsByUID");
  }
};

exports.getImpressionsByApartmentID = async (apartmentID) => {
  try {
    const docs = (
      await db
        .collection(APARTMENT_IMPRESSIONS_COLLECTION)
        .where("apartmentID", "==", apartmentID)
        .where("liked", "==", true)
        .where("endDate", "==", null)
        .get()
    ).docs;
    const data = {};
    docs.forEach((doc) => (data[doc.id] = doc.data()));

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getImpressionsByApartmentID");
  }
};
