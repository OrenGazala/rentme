const { CREDIT_SCORE } = require("../consts/collections");

const { admin } = require("../firebase-config");
const db = admin.firestore();

exports.getCreditScore = async (IDNumber) => {
  const docs = (await db.collection(CREDIT_SCORE).where("IDNumber", "==", IDNumber).get()).docs;
  const creditScore = docs[0].data().creditScore;
  return creditScore;
};
