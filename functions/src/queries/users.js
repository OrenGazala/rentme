const { USERS_COLLECTION } = require("../consts/collections");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");
const db = admin.firestore();

exports.getUser = async (uid) => {
  try {
    const user = (await db.doc(`${USERS_COLLECTION}/${uid}`).get()).data();
    return user;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getUser");
  }
};

exports.getUsers = async (uids) => {
  try {
    if (!uids) return {};

    const promisesToAwait = [];
    while (uids.length) {
      const uids10 = uids.splice(0, 10);
      const promise = db
        .collection(USERS_COLLECTION)
        .where(admin.firestore.FieldPath.documentId(), "in", uids10)
        .get();
      promisesToAwait.push(promise);
    }

    const data = {};
    const responses = await Promise.all(promisesToAwait);
    responses.forEach((response) => {
      response.docs.forEach((doc) => (data[doc.id] = doc.data()));
    });

    return data;
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "getUsers");
  }
};
