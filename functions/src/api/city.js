const axios = require("axios");
const countryIds = "US";

exports.getNearCities = async (cityId) => {
  const response = await axios({
    method: "GET",
    url: `https://wft-geo-db.p.rapidapi.com/v1/geo/cities/${cityId}/nearbyCities`,
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
      "x-rapidapi-key": "0b6aadfe4dmshef694902c1151b2p1856c0jsnb7cc36ac4c31",
      useQueryString: true,
    },
    params: {
      countryIds,
      radius: 100,
      types: "city",
      limit: "9",
    },
  });
  const cities = response.data.data.map((city) => {
    return { ...city };
  });
  return cities;
};

exports.getDistanceCities = async (cityId, fromCityId) => {
  const response = await axios({
    method: "GET",
    url: `https://wft-geo-db.p.rapidapi.com/v1/geo/cities/${cityId}/distance`,
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
      "x-rapidapi-key": "0b6aadfe4dmshef694902c1151b2p1856c0jsnb7cc36ac4c31",
      useQueryString: true,
    },
    params: {
      fromCityId,
    },
  });
  return response.data.data;
};

exports.getDistance = (lat1, lon1, lat2, lon2) => {
  const radlat1 = (Math.PI * lat1) / 180;
  const radlat2 = (Math.PI * lat2) / 180;
  const theta = lon1 - lon2;
  const radtheta = (Math.PI * theta) / 180;
  let dist =
    Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  dist = dist.toFixed(1);
  return dist;
};
