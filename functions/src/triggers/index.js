const users = require("./users");
const apartments = require("./apartments");
const apartmentsImpressions = require("./apartment-impressions");
const creditScore = require("./credit-score");

module.exports = {
  users,
  apartments,
  apartmentsImpressions,
  creditScore,
};
