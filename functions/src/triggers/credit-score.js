const { CREDIT_SCORE } = require("../consts/collections");
const { creditScoreModel } = require("../models/credit-score");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");

const db = admin.firestore();

exports.generateCreditScore = functions.firestore
  .document("users/{uid}")
  .onCreate(async (snap, context) => {
    const creditScore = creditScoreModel;
    creditScore.IDNumber = snap.data().IDNumber;
    creditScore.creditScore = getRndInteger(300, 850);

    return db.collection(CREDIT_SCORE).add(creditScore);
  });

const getRndInteger = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
