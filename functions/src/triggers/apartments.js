const { APARTMENTS_COLLECTION } = require("../consts/collections");
const { calcFeedbackDetails } = require("../calcs/feedbacks");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");
const db = admin.firestore();

exports.updateFeedbackDetails = functions.firestore
  .document("apartments/{apartmentID}/feedbacks/{feedbackID}")
  .onWrite(async (snap, context) => {
    const ref = db.doc(`${APARTMENTS_COLLECTION}/${context.params.apartmentID}`);

    const oldFeedbackDetails = (await ref.get()).data().feedbackDetails;
    const feedbackDetails = calcFeedbackDetails(oldFeedbackDetails, snap);

    return ref.update({ feedbackDetails });
  });
