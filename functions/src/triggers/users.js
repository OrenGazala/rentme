const { USERS_COLLECTION } = require("../consts/collections");
const { calcFeedbackDetails } = require("../calcs/feedbacks");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");

const db = admin.firestore();

exports.updateFeedbackDetails = functions.firestore
  .document("users/{uid}/feedbacks/{feedbackID}")
  .onWrite(async (snap, context) => {
    const ref = db.doc(`${USERS_COLLECTION}/${context.params.uid}`);

    const oldFeedbackDetails = (await ref.get()).data().feedbackDetails;
    const feedbackDetails = calcFeedbackDetails(oldFeedbackDetails, snap);

    return ref.update({ feedbackDetails });
  });
