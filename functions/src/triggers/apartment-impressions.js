const {
  USERS_COLLECTION,
  APARTMENTS_COLLECTION,
  APARTMENT_IMPRESSIONS_COLLECTION,
} = require("../consts/collections");
const { calcAgreementLevel } = require("../calcs/agreement-level");

const functions = require("firebase-functions");
const { admin } = require("../firebase-config");

const db = admin.firestore();

exports.addAgreementLevel = functions.firestore
  .document("apartment_impressions/{apartmentImpressionsId}")
  .onWrite(async (snap, context) => {
    if (!snap.after.data()) return {};

    const { uid, apartmentID, liked } = snap.after.data();

    if (liked === false) return {};

    const user = (await db.doc(`${USERS_COLLECTION}/${uid}`).get()).data();
    const apartment = (await db.doc(`${APARTMENTS_COLLECTION}/${apartmentID}`).get()).data();

    const agreementLevel = await calcAgreementLevel(user, apartment);

    return db
      .doc(`${APARTMENT_IMPRESSIONS_COLLECTION}/${context.params.apartmentImpressionsId}`)
      .update({ agreementLevel });
  });
