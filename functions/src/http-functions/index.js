const apartments = require("./apartments");
const potentialRenters = require("./potential-renters");

module.exports = {
  apartments,
  potentialRenters,
};
