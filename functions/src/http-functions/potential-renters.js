const functions = require("firebase-functions");
const { getApartmentByID } = require("../queries/apartments");
const { getUsers } = require("../queries/users");
const { getRentsByApartmentID } = require("../queries/rents");
const { getImpressionsByApartmentID } = require("../queries/apartment-impressions");
const { fromEntries } = require("../utils");

exports.getAll = functions.https.onCall(async (data, context) => {
  const { apartmentID } = data;

  const apartment = await getApartmentByID(apartmentID);
  if (!apartment) return {};

  const rents = await getRentsByApartmentID(apartmentID);
  const impressions = await getImpressionsByApartmentID(apartmentID);

  const rentsUids = [];
  Object.values(rents).forEach((rent) => rentsUids.push(rent.uid));
  const rentUsers = await getUsers(rentsUids);

  const rentsWithUsers = {};
  Object.entries(rents).forEach(([key, rent]) => {
    rentsWithUsers[key] = {
      uid: rent.uid,
      user: rentUsers[rent.uid],
    };
  });

  const impressionsUids = [];
  Object.values(impressions).forEach((impression) => impressionsUids.push(impression.uid));
  const impressionUsers = await getUsers(impressionsUids);
  const impressionUsersWithAgreementLevel = addAgreementLevel(impressionUsers, impressions);

  const impressionsWithUsers = {};
  Object.entries(impressions).forEach(([key, impression]) => {
    impressionsWithUsers[key] = {
      uid: impression.uid,
      user: impressionUsersWithAgreementLevel[impression.uid],
    };
  });

  return {
    apartmentID,
    partners: apartment.partners,
    city: apartment.city,
    address: apartment.address,
    rents: rentsWithUsers,
    impressions: impressionsWithUsers,
  };
});

const addAgreementLevel = (impressionUsers, impressions) => {
  try {
    return fromEntries(
      Object.entries(impressionUsers).map(([key, value]) => {
        const impression = Object.values(impressions).find((impression) => impression.uid === key);
        if (impression) value.agreementLevel = impression.agreementLevel;
        return [key, value];
      })
    );
  } catch (err) {
    throw new functions.https.HttpsError("not-found", err.message, "addAgreementLevel");
  }
};
