const { calcMatchLevel } = require("../calcs/match-level");
const {
  getApartmentsByRenterProps,
  getRandomApartments,
  getApartmentsByIds,
} = require("../queries/apartments");
const { getAllLikeApartmentsByUID } = require("../queries/apartment-impressions");
const { getUser } = require("../queries/users");
const { getRentsByUID } = require("../queries/rents");
const { fromEntries } = require("../utils");

const functions = require("firebase-functions");

exports.getByRenterProps = functions.https.onCall(async (data, context) => {
  if (!context.auth) return {};

  const { uid } = context.auth;
  const user = await getUser(uid);
  let apartments = await getApartmentsByRenterProps(
    {
      uid,
      region: user.renterProps.city.region,
      propertyType: user.renterProps.propertyType,
      price: user.renterProps.priceLimit * 1.5,
      bedrooms: [user.renterProps.bedrooms - 2, user.renterProps.bedrooms + 2],
    },
    false
  );

  const apartmentsLength = Object.keys(apartments).length;
  if (apartmentsLength < 9) {
    moreApartments = await getRandomApartments(uid, 9 - apartmentsLength, apartments);
    apartments = { ...apartments, ...moreApartments };
  }

  const apartmentsMoreData = await addMoreDataToApartments(user, apartments);
  const orderApartments = sortApartmentsByMatchLevel(apartmentsMoreData);

  return orderApartments;
});

exports.getByFilter = functions.https.onCall(async (data, context) => {
  if (!context.auth) throw new functions.https.HttpsError("unauthenticated", "No auth login");

  const { uid } = context.auth;
  const user = await getUser(uid);

  const apartments = await getApartmentsByRenterProps({ uid, ...data });

  if (!apartments) return {};
  const apartmentsMoreData = await addMoreDataToApartments(user, apartments);
  const orderApartments = sortApartmentsByMatchLevel(apartmentsMoreData);

  return orderApartments;
});

exports.getByRandom = functions.https.onCall(async (data, context) => {
  const uid = context.auth && context.auth.uid;
  return await getRandomApartments(uid, 9);
});

exports.getLikedApartments = functions.https.onCall(async (data, context) => {
  if (!context.auth) return {};
  const { uid } = context.auth;
  const user = await getUser(uid);

  const impressions = await getAllLikeApartmentsByUID(uid);

  const likedApartmentIds = Object.values(impressions).map((impression) => impression.apartmentID);
  const likedApartments = await getApartmentsByIds(likedApartmentIds);

  const apartments = {};
  Object.entries(likedApartments).forEach(([id, likedApartment]) => {
    const [key, impression] = Object.entries(impressions).find(([key, impression]) =>
      id === impression.apartmentID ? [key, impression] : false
    );

    apartments[id] = {
      ...likedApartment,
      impression: { key, value: impression },
    };
  });

  const apartmentsMoreData = await addMoreDataToApartments(user, apartments);

  return apartmentsMoreData;
});

exports.getMyRents = functions.https.onCall(async (data, context) => {
  if (!context.auth) return {};
  const { uid } = context.auth;

  const rents = await getRentsByUID(uid);

  const rentsApartmentIds = Object.values(rents).map((rent) => rent.apartmentID);
  const rentsApartments = await getApartmentsByIds(rentsApartmentIds);

  return rentsApartments;
});

const addMoreDataToApartments = async (user, apartments) => {
  const newApartments = { ...apartments };
  if (user.renterProps.city.id) {
    Object.entries(newApartments).forEach(async ([apartmentID, apartment]) => {
      newApartments[apartmentID].matchLevel = calcMatchLevel(user, apartment);
    });
  }
  return newApartments;
};

const sortApartmentsByMatchLevel = (apartments) => {
  return fromEntries(
    Object.entries(apartments).sort(
      ([key1, apartment1], [key2, apartment2]) => apartment2.matchLevel - apartment1.matchLevel
    )
  );
};
