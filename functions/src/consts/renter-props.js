exports.PROPERTY_TYPES = {
  APARTMENT: "Apartment",
  PENTHOUSE: "Penthouse",
  DUPLEX: "Duplex",
  GARDEN_APARTMENT: "Garden Apartment",
  VILA: "Vila",
};

exports.SALARIES = {
  VERY_LOW: "0 ~ 4K",
  LOW: "4K ~ 8K",
  MEDIUM: "8K ~ 12K",
  HIGH: "12K ~ 15K",
  VERY_HIGH: "15K+",
};

exports.EDUCATIONS = {
  ELEMENTRY_SCHOOL: "Elementry school",
  HIGH_SCHOOL: "High School",
  COLLEAGE: "College",
  BACHELOR_DEGREE: "Bachelor degree",
  ASSOCIATE_DEGREE: "Associate degree",
  DOCTORATE_DEGREE: "Doctorate degree",
};

exports.FAMILY_STATUSES = {
  SINGLE: "Single",
  RELATIONSHIP: "Relationship",
  MARRIED: "Married",
  DIVORCED: "Divorced",
  WIDOW: "Widow",
};
