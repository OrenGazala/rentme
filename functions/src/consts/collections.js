exports.USERS_COLLECTION = "users";
exports.APARTMENTS_COLLECTION = "apartments";
exports.APARTMENT_IMPRESSIONS_COLLECTION = "apartment_impressions";
exports.FEEDBACKS_COLLECTION = "feedbacks";
exports.CREDIT_SCORE = "credit_score";
exports.RENTS_COLLECTION = "rents";
